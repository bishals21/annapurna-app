# annapurna_app

A new Flutter project.

Annapurna post is a leading broadsheet Nepali daily newspaper. It
started in 2002 December. It's owned and managed by Nepal News Network
International Pvt Ltd, Tinkune, Kathmandu Nepal. Annapurna post is
publishing from on Kathmandu, Butwal and Itahari. 3NI has online news
site: www.annapurnapost.com.

We have also introduced mobile application to provide time to time
update on current events and issues on politicies, society,
entertainment, economy and sports and many more.


## Getting Started

For help getting started with Flutter, view our online
[documentation](https://flutter.io/).
