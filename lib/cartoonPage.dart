import 'dart:async';
import 'dart:convert';

import 'package:annapurna_app/appText.dart';
import 'package:annapurna_app/apputil.dart';
import 'package:annapurna_app/model/cartoonModel.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class CartoonPage extends StatefulWidget {
  @override
  _CartoonPageState createState() => _CartoonPageState();
}

class _CartoonPageState extends State<CartoonPage> {
  List<CartoonModel> list = new List();

  Future<Null> _getCartoons() async {
    bool isConnected = await AppUtil.checkConnection();
    final Completer<Null> completer = new Completer<Null>();
    print(isConnected);
    if (isConnected) {
      http
          .get(AppText.baseURl + "illustration/{id}/detail?_format=json")
          .then((response) {
        print("Response Body:${response.body}");
        if (response.statusCode == 200) {
          List<CartoonModel> cartoonList = new List();
          var responseBody = json.decode(response.body);
          var relatedData = responseBody['relatedData'];
          for (var item in relatedData) {
            cartoonList.add(
                new CartoonModel(id: item['id'], imageURl: item['imageUrl']));
          }

          if (mounted) {
            this.setState(() {
              list = cartoonList;
              completer.complete(null);
            });
          }
          return completer.future;
        }
      });
    } else {
      print("No internet connection");
      return completer.future;
    }
  }

  @override
  void initState() {
    _getCartoons();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("कार्टून"),
      ),
      body: GridView.count(
        padding: EdgeInsets.all(8.0),
        crossAxisCount: 2,
        crossAxisSpacing: 8.0,
        mainAxisSpacing: 8.0,
        children: list.map((item) {
          return Container(
            decoration: BoxDecoration(
              border: Border.all(
                color: Colors.grey,
              ),
            ),
            child: InkWell(
              onTap: () {
                Navigator.push(context, new MaterialPageRoute<Null>(
                    builder: (BuildContext context) {
                  return new Material(
                    color: Colors.blue.withOpacity(0.25),
                    child: InkWell(
                      onTap: () {
                        Navigator.pop(context);
                      },
                      child: new Hero(
                        tag: "hero-image${item.id}",
                        child: Container(
                          child: CachedNetworkImage(
                            imageUrl: AppText.prefixImage + item.imageURl,
                            placeholder: (_, url) => new Image.asset(
                              "assets/defaultImage.jpg",
                              fit: BoxFit.cover,
                            ),
                            errorWidget: (_, url, error) => new Image.asset(
                              "assets/defaultImage.jpg",
                              fit: BoxFit.cover,
                            ),
                            fadeOutDuration: new Duration(seconds: 1),
                            fadeInDuration: new Duration(seconds: 2),
                            fit: BoxFit.contain,
                          ),
                        ),
                      ),
                    ),
                  );
                }));
              },
              child: Hero(
                tag: "hero-image${item.id}",
                child: Container(
                  child: CachedNetworkImage(
                    imageUrl: AppText.prefixImage + item.imageURl,
                    placeholder: (_, url) => new Image.asset(
                      "assets/defaultImage.jpg",
                      fit: BoxFit.cover,
                    ),
                    errorWidget: (_, url, error) => new Image.asset(
                      "assets/defaultImage.jpg",
                      fit: BoxFit.cover,
                    ),
                    fadeOutDuration: new Duration(seconds: 1),
                    fadeInDuration: new Duration(seconds: 2),
                    fit: BoxFit.cover,
                  ),
                ),
              ),
            ),
          );
        }).toList(),
      ),
    );
  }
}

class HeroDialogRoute<T> extends PageRoute<T> {
  HeroDialogRoute({this.builder}) : super();

  final WidgetBuilder builder;

  @override
  bool get opaque => false;

  @override
  bool get barrierDismissible => true;

  @override
  Duration get transitionDuration => const Duration(milliseconds: 300);

  @override
  bool get maintainState => true;

  @override
  Color get barrierColor => Colors.black54;

  @override
  Widget buildTransitions(BuildContext context, Animation<double> animation,
      Animation<double> secondaryAnimation, Widget child) {
    return new FadeTransition(
        opacity: new CurvedAnimation(parent: animation, curve: Curves.easeOut),
        child: child);
  }

  @override
  Widget buildPage(BuildContext context, Animation<double> animation,
      Animation<double> secondaryAnimation) {
    return builder(context);
  }

  // TODO: implement barrierLabel
  @override
  String get barrierLabel => null;
}
