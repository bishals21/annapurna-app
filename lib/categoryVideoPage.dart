import 'dart:async';
import 'dart:convert';

import 'package:annapurna_app/appText.dart';
import 'package:annapurna_app/apputil.dart';
import 'package:annapurna_app/model/newsItem.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:flutter_youtube/flutter_youtube.dart';

class CategoryVideoPage extends StatefulWidget {
  @override
  _CategoryVideoPageState createState() => _CategoryVideoPageState();
}

class _CategoryVideoPageState extends State<CategoryVideoPage> {
  List<NewsItem> newsList = new List();
  int page = 1;
  bool hasMore = false;

  Future requestLoadMore;

  Future<Null> _handleRefresh() async {
    print('_handleRefresh');
    final Completer<Null> completer = new Completer<Null>();
    List<NewsItem> listOfItem = new List();
    bool isConnected = await AppUtil.checkConnection();
    if (isConnected) {
      http
          .get(
        AppText.baseURl +
            "news/list?_format=json&page=" +
            page.toString() +
            "&per_page=15&category_alias=multimedia",
      )
          .then((response) {
        if (response.statusCode == 200) {
          var responseBody = json.decode(response.body);
          var data = responseBody["data"];
          var categoryMain = responseBody["categoryMain"];
          int totalPage = responseBody["totalPage"];
          for (var item in data) {
            listOfItem.add(new NewsItem(
              id: item['id'],
              title: item['title'],
              introText: item['introText'],
              featuredImage: item['featuredImage'].toString(),
              videoLink: item['videoLink'],
              publishOn: item['publishOn'],
              authorName: item['author']['name'].toString(),
              categoryName: item['categories'].isNotEmpty
                  ? item['categories'][0]['name']
                  : "",
            ));
          }
          if (categoryMain.length > 1) {
            listOfItem.insert(
                0,
                new NewsItem(
                  id: categoryMain[0]['id'],
                  title: categoryMain[0]['title'],
                  introText: categoryMain[0]['introText'],
                  featuredImage: categoryMain[0]['featuredImage'].toString(),
                  videoLink: categoryMain[0]['videoLink'],
                  publishOn: categoryMain[0]['publishOn'],
                  authorName: categoryMain[0]['author']['name'].toString(),
                  categoryName: categoryMain[0]['categories'].isNotEmpty
                      ? categoryMain[0]['categories'][0]['name']
                      : "",
                ));
          }

          if (categoryMain.length >= 2 && listOfItem.length >= 8) {
            listOfItem.insert(
                8,
                new NewsItem(
                  id: categoryMain[1]['id'],
                  title: categoryMain[1]['title'],
                  introText: categoryMain[1]['introText'],
                  featuredImage: categoryMain[1]['featuredImage'].toString(),
                  videoLink: categoryMain[1]['videoLink'],
                  publishOn: categoryMain[1]['publishOn'],
                  authorName: categoryMain[1]['author']['name'].toString(),
                  categoryName: categoryMain[1]['categories'].isNotEmpty
                      ? categoryMain[1]['categories'][0]['name']
                      : "",
                ));
          }

          if (mounted) {
            this.setState(() {
              hasMore = page == totalPage;
              print(hasMore);
              newsList = listOfItem;
              completer.complete(null);
            });
          }

          return completer.future;
        }
      });
    } else {
      print("No Internet Connection");
      return completer.future;
    }
  }

  Future<Null> _handleLoadMore() async {
    bool isConnected = await AppUtil.checkConnection();

    final Completer<Null> completer = new Completer<Null>();
    if (isConnected) {
      page = page + 1;
      int newPage = page;
      newsList.add(null);
      var nextPageUrl = AppText.baseURl +
          "news/list?_format=json&page=" +
          newPage.toString() +
          "&per_page=15&category_alias=multimedia";

      print('_handleLoadMore' + nextPageUrl);
      List<NewsItem> listOfItem = new List();

      http
          .get(
        nextPageUrl,
      )
          .then((response) {
        if (response.statusCode == 200) {
          var responseBody = json.decode(response.body);
          var data = responseBody["data"];
          int totalPage = responseBody["totalPage"];
          for (var item in data) {
            listOfItem.add(new NewsItem(
              id: item['id'],
              title: item['title'],
              introText: item['introText'],
              featuredImage: item['featuredImage'].toString(),
              videoLink: item['videoLink'],
              publishOn: item['publishOn'],
              authorName: item['author']['name'].toString(),
              categoryName: item['categories'].isNotEmpty
                  ? item['categories'][0]['name']
                  : "",
            ));
          }

          if (mounted) {
            this.setState(() {
              hasMore = page == totalPage;
              print(hasMore);
              newsList.removeLast();
              newsList.addAll(listOfItem);
              completer.complete(null);
            });
          }
        }
      });
    } else {
      print("No Internet Connection");
      return completer.future;
    }

    return completer.future;
  }

  lockedLoadNext() {
    if (this.requestLoadMore == null) {
      this.requestLoadMore = _handleLoadMore().then((x) {
        this.requestLoadMore = null;
      });
    }
  }

  @override
  void initState() {
    _handleRefresh();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("भिडियो"),
      ),
      body: CustomScrollView(
        slivers: <Widget>[
          SliverList(
            delegate: SliverChildBuilderDelegate((context, index) {
              if (index + 4 > newsList.length) {
                if (hasMore == false) {
                  lockedLoadNext();
                }
              }
              if (index == 0) {
                return InkWell(
                  onTap: () {
                    FlutterYoutube.playYoutubeVideoByUrl(
                      apiKey: AppText.googleApiKey,
                      videoUrl: "https://youtu.be/4fdyw3jqj1M",
                    );
                  },
                  child: Container(
                    height: 270.0,
                    margin: EdgeInsets.only(
                      bottom: 4.0,
                    ),
                    child: Stack(
                      children: <Widget>[
                        Hero(
                          tag: "hero-image${newsList[index].id}",
                          child: Container(
                            height: 270.0,
                            child: CachedNetworkImage(
                              imageUrl: AppText.prefixImage +
                                  newsList[index].featuredImage,
                              placeholder: (_, url) => new Image.asset(
                                "assets/defaultImage.jpg",
                                fit: BoxFit.cover,
                              ),
                              errorWidget: (_, url,error) => new Image.asset(
                                "assets/defaultImage.jpg",
                                fit: BoxFit.cover,
                              ),
                              fadeOutDuration: new Duration(seconds: 1),
                              fadeInDuration: new Duration(seconds: 2),
                              fit: BoxFit.cover,
                            ),
                          ),
                        ),
                        Container(
                          decoration: BoxDecoration(
                            gradient: LinearGradient(
                              begin: Alignment.topCenter,
                              end: Alignment.bottomCenter,
                              colors: [
                                const Color(0x00000000),
                                const Color(0x00000000),
                                const Color(0x00000000),
                                const Color(0x99000000),
                                const Color(0xE6000000),
                                const Color(0xE6000000),
                              ],
                            ),
                          ),
                        ),
                        Positioned(
                          bottom: 0.0,
                          left: 0.0,
                          right: 0.0,
                          child: Container(
                            padding: EdgeInsets.all(8.0),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  newsList[index].title,
                                  maxLines: 2,
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 18.0,
                                  ),
                                ),
                                Row(
                                  children: <Widget>[
                                    Text(
                                      newsList[index].authorName +
                                          "   |   " +
                                          newsList[index].publishOn,
                                      style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 15.0,
                                      ),
                                    ),
                                    Flexible(
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.end,
                                        children: <Widget>[
                                          IconButton(
                                            padding: EdgeInsets.all(0.0),
                                            icon: Icon(Icons.bookmark_border),
                                            color: Colors.white,
                                            onPressed: () {},
                                          ),
                                          IconButton(
                                            padding: EdgeInsets.all(0.0),
                                            icon: Icon(Icons.share),
                                            color: Colors.white,
                                            onPressed: () {},
                                          ),
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ),
                        ),
                        Positioned(
                          bottom: 40.0,
                          left: 0.0,
                          right: 0.0,
                          top: 0.0,
                          child: Center(
                            child: Image.asset(
                              "assets/playButton.png",
                              height: 75.0,
                              width: 75.0,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                );
              } else if (index > 0 && index < 4 && newsList.length > 0) {
                return InkWell(
                  onTap: () {
                    // Navigator.push(
                    //     context,
                    //     MaterialPageRoute(
                    //         builder: (context) => NewsDetailPage(
                    //               newsItem: newsList[index],
                    //             )));
                  },
                  child: Card(
                    elevation: 2.0,
                    margin: EdgeInsets.all(4.0),
                    child: Container(
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Hero(
                            tag: index == 1
                                ? "hero-image${newsList[1].id}"
                                : index == 2
                                    ? "hero-image${newsList[2].id}"
                                    : "hero-image${newsList[3].id}",
                            child: Container(
                              height: 90.0,
                              width: 120.0,
                              child: Stack(
                                children: <Widget>[
                                  Container(
                                    height: 90.0,
                                    child: CachedNetworkImage(
                                      imageUrl: AppText.prefixImage +
                                          newsList[index].featuredImage,
                                      placeholder: (_, url) => new Image.asset(
                                        "assets/defaultImage.jpg",
                                        fit: BoxFit.cover,
                                      ),
                                      errorWidget: (_, url,error) => new Image.asset(
                                        "assets/defaultImage.jpg",
                                        fit: BoxFit.cover,
                                      ),
                                      fadeOutDuration: new Duration(seconds: 1),
                                      fadeInDuration: new Duration(seconds: 2),
                                      fit: BoxFit.cover,
                                    ),
                                  ),
                                  Positioned(
                                    bottom: 0.0,
                                    left: 0.0,
                                    right: 0.0,
                                    top: 0.0,
                                    child: Center(
                                      child: Image.asset(
                                        "assets/playButton.png",
                                        height: 45.0,
                                        width: 45.0,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                          Flexible(
                            child: Padding(
                              padding: EdgeInsets.all(8.0),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Text(
                                    newsList[index].title,
                                    overflow: TextOverflow.ellipsis,
                                    maxLines: 2,
                                    style: TextStyle(
                                      fontSize: 18.0,
                                      color: const Color(0xFF4D4E4E),
                                    ),
                                  ),
                                  Text(
                                    newsList[index].publishOn,
                                    overflow: TextOverflow.ellipsis,
                                    maxLines: 2,
                                    style: TextStyle(
                                      color: const Color(0xFF808080),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                );
              } else if (index == 4 && newsList.length >= 6) {
                return Container(
                  height: 175.0,
                  margin: EdgeInsets.only(
                    top: 4.0,
                    bottom: 4.0,
                    left: 4.0,
                    right: 4.0,
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      Flexible(
                        flex: 1,
                        child: InkWell(
                          onTap: () {
                            // Navigator.push(
                            //     context,
                            //     MaterialPageRoute(
                            //         builder: (context) => NewsDetailPage(
                            //               newsItem: newsList[4],
                            //             )));
                          },
                          child: Container(
                            margin: EdgeInsets.only(
                              right: 4.0,
                            ),
                            decoration: BoxDecoration(
                              border: Border(
                                bottom: BorderSide(
                                  color: const Color(0xFF651FFF),
                                  width: 5.0,
                                ),
                              ),
                              color: Colors.white,
                              boxShadow: [
                                BoxShadow(
                                  color: const Color(0x26000000),
                                  blurRadius: 3.0,
                                  spreadRadius: 1.0,
                                  offset: new Offset(1.0, 0.0),
                                ),
                              ],
                            ),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Hero(
                                  tag: "hero-image${newsList[4].id}",
                                  child: Container(
                                    height: 100.0,
                                    child: Stack(
                                      children: <Widget>[
                                        Container(
                                          height: 100.0,
                                          child: CachedNetworkImage(
                                            imageUrl: AppText.prefixImage +
                                                newsList[4].featuredImage,
                                            placeholder: (_, url) => new Image.asset(
                                              "assets/defaultImage.jpg",
                                              fit: BoxFit.cover,
                                            ),
                                            errorWidget: (_, url,error) => new Image.asset(
                                              "assets/defaultImage.jpg",
                                              fit: BoxFit.cover,
                                            ),
                                            fadeOutDuration:
                                                new Duration(seconds: 1),
                                            fadeInDuration:
                                                new Duration(seconds: 2),
                                            fit: BoxFit.cover,
                                          ),
                                        ),
                                        Positioned(
                                          bottom: 0.0,
                                          left: 0.0,
                                          right: 0.0,
                                          top: 0.0,
                                          child: Center(
                                            child: Image.asset(
                                              "assets/playButton.png",
                                              height: 45.0,
                                              width: 45.0,
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                                Container(
                                  padding: EdgeInsets.all(8.0),
                                  child: Text(
                                    newsList[4].title,
                                    maxLines: 2,
                                    overflow: TextOverflow.ellipsis,
                                    style: TextStyle(
                                      color: const Color(0xFF4D4E4E),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                      Flexible(
                        flex: 1,
                        child: InkWell(
                          onTap: () {
                            // Navigator.push(
                            //     context,
                            //     MaterialPageRoute(
                            //         builder: (context) => NewsDetailPage(
                            //               newsItem: newsList[5],
                            //             )));
                          },
                          child: Container(
                            margin: EdgeInsets.only(
                              left: 4.0,
                            ),
                            decoration: BoxDecoration(
                              border: Border(
                                bottom: BorderSide(
                                  color: const Color(0xFF651FFF),
                                  width: 5.0,
                                ),
                              ),
                              color: Colors.white,
                              boxShadow: [
                                BoxShadow(
                                  color: const Color(0x26000000),
                                  blurRadius: 3.0,
                                  spreadRadius: 1.0,
                                  offset: new Offset(1.0, 0.0),
                                ),
                              ],
                            ),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Hero(
                                  tag: "hero-image${newsList[5].id}",
                                  child: Container(
                                    height: 100.0,
                                    child: Stack(
                                      children: <Widget>[
                                        Container(
                                          height: 100.0,
                                          child: CachedNetworkImage(
                                            imageUrl: AppText.prefixImage +
                                                newsList[5].featuredImage,
                                            placeholder: (_, url) => new Image.asset(
                                              "assets/defaultImage.jpg",
                                              fit: BoxFit.cover,
                                            ),
                                            errorWidget: (_, url,error) => new Image.asset(
                                              "assets/defaultImage.jpg",
                                              fit: BoxFit.cover,
                                            ),
                                            fadeOutDuration:
                                                new Duration(seconds: 1),
                                            fadeInDuration:
                                                new Duration(seconds: 2),
                                            fit: BoxFit.cover,
                                          ),
                                        ),
                                        Positioned(
                                          bottom: 0.0,
                                          left: 0.0,
                                          right: 0.0,
                                          top: 0.0,
                                          child: Center(
                                            child: Image.asset(
                                              "assets/playButton.png",
                                              height: 45.0,
                                              width: 45.0,
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                                Container(
                                  padding: EdgeInsets.all(8.0),
                                  child: Text(
                                    newsList[5].title,
                                    maxLines: 2,
                                    overflow: TextOverflow.ellipsis,
                                    style: TextStyle(
                                      color: const Color(0xFF4D4E4E),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                );
              } else if (index == 5 && newsList.length >= 8) {
                return Container(
                  height: 175.0,
                  margin: EdgeInsets.only(
                    top: 4.0,
                    bottom: 8.0,
                    left: 4.0,
                    right: 4.0,
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      Flexible(
                        flex: 1,
                        child: InkWell(
                          onTap: () {
                            // Navigator.push(
                            //     context,
                            //     MaterialPageRoute(
                            //         builder: (context) => NewsDetailPage(
                            //               newsItem: newsList[6],
                            //             )));
                          },
                          child: Container(
                            margin: EdgeInsets.only(
                              right: 4.0,
                            ),
                            decoration: BoxDecoration(
                              border: Border(
                                bottom: BorderSide(
                                  color: const Color(0xFF651FFF),
                                  width: 5.0,
                                ),
                              ),
                              color: Colors.white,
                              boxShadow: [
                                BoxShadow(
                                  color: const Color(0x26000000),
                                  blurRadius: 3.0,
                                  spreadRadius: 1.0,
                                  offset: new Offset(1.0, 0.0),
                                ),
                              ],
                            ),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Hero(
                                  tag: "hero-image${newsList[6].id}",
                                  child: Container(
                                    height: 100.0,
                                    child: Stack(
                                      children: <Widget>[
                                        Container(
                                          height: 100.0,
                                          child: CachedNetworkImage(
                                            imageUrl: AppText.prefixImage +
                                                newsList[6].featuredImage,
                                            placeholder: (_, url) => new Image.asset(
                                              "assets/defaultImage.jpg",
                                              fit: BoxFit.cover,
                                            ),
                                            errorWidget: (_, url,error) => new Image.asset(
                                              "assets/defaultImage.jpg",
                                              fit: BoxFit.cover,
                                            ),
                                            fadeOutDuration:
                                                new Duration(seconds: 1),
                                            fadeInDuration:
                                                new Duration(seconds: 2),
                                            fit: BoxFit.cover,
                                          ),
                                        ),
                                        Positioned(
                                          bottom: 0.0,
                                          left: 0.0,
                                          right: 0.0,
                                          top: 0.0,
                                          child: Center(
                                            child: Image.asset(
                                              "assets/playButton.png",
                                              height: 45.0,
                                              width: 45.0,
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                                Container(
                                  padding: EdgeInsets.all(8.0),
                                  child: Text(
                                    newsList[6].title,
                                    maxLines: 2,
                                    overflow: TextOverflow.ellipsis,
                                    style: TextStyle(
                                      color: const Color(0xFF4D4E4E),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                      Flexible(
                        flex: 1,
                        child: InkWell(
                          onTap: () {
                            // Navigator.push(
                            //     context,
                            //     MaterialPageRoute(
                            //         builder: (context) => NewsDetailPage(
                            //               newsItem: newsList[7],
                            //             )));
                          },
                          child: Container(
                            margin: EdgeInsets.only(
                              left: 4.0,
                            ),
                            decoration: BoxDecoration(
                              border: Border(
                                bottom: BorderSide(
                                  color: const Color(0xFF651FFF),
                                  width: 5.0,
                                ),
                              ),
                              color: Colors.white,
                              boxShadow: [
                                BoxShadow(
                                  color: const Color(0x26000000),
                                  blurRadius: 3.0,
                                  spreadRadius: 1.0,
                                  offset: new Offset(1.0, 0.0),
                                ),
                              ],
                            ),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Hero(
                                  tag: "hero-image${newsList[7].id}",
                                  child: Container(
                                    height: 100.0,
                                    child: Stack(
                                      children: <Widget>[
                                        Container(
                                          height: 100.0,
                                          child: CachedNetworkImage(
                                            imageUrl: AppText.prefixImage +
                                                newsList[7].featuredImage,
                                            placeholder: (_, url) => new Image.asset(
                                              "assets/defaultImage.jpg",
                                              fit: BoxFit.cover,
                                            ),
                                            errorWidget: (_, url,error) => new Image.asset(
                                              "assets/defaultImage.jpg",
                                              fit: BoxFit.cover,
                                            ),
                                            fadeOutDuration:
                                                new Duration(seconds: 1),
                                            fadeInDuration:
                                                new Duration(seconds: 2),
                                            fit: BoxFit.cover,
                                          ),
                                        ),
                                        Positioned(
                                          bottom: 0.0,
                                          left: 0.0,
                                          right: 0.0,
                                          top: 0.0,
                                          child: Center(
                                            child: Image.asset(
                                              "assets/playButton.png",
                                              height: 45.0,
                                              width: 45.0,
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                                Container(
                                  padding: EdgeInsets.all(8.0),
                                  child: Text(
                                    newsList[7].title,
                                    maxLines: 2,
                                    overflow: TextOverflow.ellipsis,
                                    style: TextStyle(
                                      color: const Color(0xFF4D4E4E),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                );
              } else if (index == 6 && newsList.length >= 9) {
                return InkWell(
                  onTap: () {
                    // Navigator.push(
                    //     context,
                    //     MaterialPageRoute(
                    //         builder: (context) => NewsDetailPage(
                    //               newsItem: newsList[8],
                    //             )));
                  },
                  child: Container(
                    height: 270.0,
                    margin: EdgeInsets.only(
                      bottom: 4.0,
                    ),
                    child: Stack(
                      children: <Widget>[
                        Hero(
                          tag: "hero-image${newsList[8].id}",
                          child: Container(
                            height: 270.0,
                            child: CachedNetworkImage(
                              imageUrl: AppText.prefixImage +
                                  newsList[8].featuredImage,
                              placeholder: (_, url) => new Image.asset(
                                "assets/defaultImage.jpg",
                                fit: BoxFit.cover,
                              ),
                              errorWidget: (_, url,error) => new Image.asset(
                                "assets/defaultImage.jpg",
                                fit: BoxFit.cover,
                              ),
                              fadeOutDuration: new Duration(seconds: 1),
                              fadeInDuration: new Duration(seconds: 2),
                              fit: BoxFit.cover,
                            ),
                          ),
                        ),
                        Container(
                          decoration: BoxDecoration(
                            gradient: LinearGradient(
                              begin: Alignment.topCenter,
                              end: Alignment.bottomCenter,
                              colors: [
                                const Color(0x00000000),
                                const Color(0x00000000),
                                const Color(0x00000000),
                                const Color(0x99000000),
                                //const Color(0xB3000000),
                                const Color(0xE6000000),
                                const Color(0xE6000000),
                              ],
                            ),
                          ),
                        ),
                        Positioned(
                          bottom: 0.0,
                          left: 0.0,
                          right: 0.0,
                          child: Container(
                            padding: EdgeInsets.all(8.0),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  newsList[8].title,
                                  maxLines: 2,
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 18.0,
                                  ),
                                ),
                                Row(
                                  children: <Widget>[
                                    Text(
                                      newsList[8].authorName +
                                          "   |   " +
                                          newsList[8].publishOn,
                                      style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 15.0,
                                      ),
                                    ),
                                    Flexible(
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.end,
                                        children: <Widget>[
                                          IconButton(
                                            padding: EdgeInsets.all(0.0),
                                            icon: Icon(Icons.bookmark_border),
                                            color: Colors.white,
                                            onPressed: () {},
                                          ),
                                          IconButton(
                                            padding: EdgeInsets.all(0.0),
                                            icon: Icon(Icons.share),
                                            color: Colors.white,
                                            onPressed: () {},
                                          ),
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ),
                        ),
                        Positioned(
                          bottom: 0.0,
                          left: 0.0,
                          right: 0.0,
                          top: 0.0,
                          child: Center(
                            child: Image.asset(
                              "assets/playButton.png",
                              height: 75.0,
                              width: 75.0,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                );
              } else if (index > 6) {
                return InkWell(
                  onTap: () {
                    // Navigator.push(
                    //     context,
                    //     MaterialPageRoute(
                    //         builder: (context) => NewsDetailPage(
                    //               newsItem: newsList[index + 2],
                    //             )));
                  },
                  child: Card(
                    elevation: 2.0,
                    child: Container(
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Hero(
                            tag: "hero-image${newsList[index + 2].id}",
                            child: Container(
                              height: 90.0,
                              width: 120.0,
                              child: Stack(
                                children: <Widget>[
                                  Container(
                                    height: 90.0,
                                    child: CachedNetworkImage(
                                      imageUrl: AppText.prefixImage +
                                          newsList[index + 2].featuredImage,
                                      placeholder: (_, url) => new Image.asset(
                                        "assets/defaultImage.jpg",
                                        fit: BoxFit.cover,
                                      ),
                                      errorWidget: (_, url,error) => new Image.asset(
                                        "assets/defaultImage.jpg",
                                        fit: BoxFit.cover,
                                      ),
                                      fadeOutDuration: new Duration(seconds: 1),
                                      fadeInDuration: new Duration(seconds: 2),
                                      fit: BoxFit.cover,
                                    ),
                                  ),
                                  Positioned(
                                    bottom: 0.0,
                                    left: 0.0,
                                    right: 0.0,
                                    top: 0.0,
                                    child: Center(
                                      child: Image.asset(
                                        "assets/playButton.png",
                                        height: 45.0,
                                        width: 45.0,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                          Flexible(
                            child: Padding(
                              padding: EdgeInsets.all(8.0),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Text(
                                    newsList[index + 2].title,
                                    overflow: TextOverflow.ellipsis,
                                    maxLines: 2,
                                    style: TextStyle(
                                      fontSize: 18.0,
                                      color: const Color(0xFF4D4E4E),
                                    ),
                                  ),
                                  Text(
                                    newsList[index + 2].publishOn,
                                    overflow: TextOverflow.ellipsis,
                                    maxLines: 2,
                                    style: TextStyle(
                                      color: const Color(0xFF808080),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                );
              } else if (newsList[index] == null) {
                return new Center(
                  child: new CircularProgressIndicator(),
                );
              }
            }, childCount: newsList.length - 2),
          ),
        ],
      ),
    );
  }
}
