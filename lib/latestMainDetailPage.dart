import 'package:annapurna_app/appText.dart';
import 'package:annapurna_app/model/newsItem.dart';
import 'package:annapurna_app/newsDetailPage.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

class LatestMainDetailPage extends StatefulWidget {
  final List<NewsItem> newsList;
  final String title;
  LatestMainDetailPage({Key key, this.newsList, this.title}) : super(key: key);
  @override
  _LatestMainDetailPageState createState() => _LatestMainDetailPageState();
}

class _LatestMainDetailPageState extends State<LatestMainDetailPage> {
  List<NewsItem> newsList = new List();
  String title = "";

  @override
  void initState() {
    newsList = widget.newsList;
    title = widget.title;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(title),
      ),
      body: CustomScrollView(
        slivers: <Widget>[
          SliverList(
            delegate: SliverChildBuilderDelegate((context, index) {
              if (index == 0) {
                return InkWell(
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => NewsDetailPage(
                                  newsItem: newsList[index],
                                )));
                  },
                  child: Container(
                    height: 270.0,
                    margin: EdgeInsets.only(
                      bottom: 4.0,
                    ),
                    child: Stack(
                      children: <Widget>[
                        Hero(
                          tag: "hero-image${newsList[index].id}",
                          child: Container(
                            height: 270.0,
                            child: CachedNetworkImage(
                              imageUrl: AppText.prefixImage +
                                  newsList[index].featuredImage,
                              placeholder: (_, url) => new Image.asset(
                                "assets/defaultImage.jpg",
                                fit: BoxFit.cover,
                              ),
                              errorWidget: (_, url, error) => new Image.asset(
                                "assets/defaultImage.jpg",
                                fit: BoxFit.cover,
                              ),
                              fadeOutDuration: new Duration(seconds: 1),
                              fadeInDuration: new Duration(seconds: 2),
                              fit: BoxFit.cover,
                            ),
                          ),
                        ),
                        Container(
                          decoration: BoxDecoration(
                            gradient: LinearGradient(
                              begin: Alignment.topCenter,
                              end: Alignment.bottomCenter,
                              colors: [
                                const Color(0x00000000),
                                const Color(0x00000000),
                                const Color(0x00000000),
                                const Color(0x99000000),
                                const Color(0xE6000000),
                                const Color(0xE6000000),
                              ],
                            ),
                          ),
                        ),
                        Positioned(
                          bottom: 0.0,
                          left: 0.0,
                          right: 0.0,
                          child: Container(
                            padding: EdgeInsets.all(8.0),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  newsList[index].title,
                                  maxLines: 2,
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 18.0,
                                  ),
                                ),
                                Row(
                                  children: <Widget>[
                                    Text(
                                      newsList[index].authorName +
                                          "   |   " +
                                          newsList[index].publishOn,
                                      style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 15.0,
                                      ),
                                    ),
                                    Flexible(
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.end,
                                        children: <Widget>[
                                          IconButton(
                                            padding: EdgeInsets.all(0.0),
                                            icon: Icon(Icons.bookmark_border),
                                            color: Colors.white,
                                            onPressed: () {},
                                          ),
                                          IconButton(
                                            padding: EdgeInsets.all(0.0),
                                            icon: Icon(Icons.share),
                                            color: Colors.white,
                                            onPressed: () {},
                                          ),
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                );
              } else if (index > 0 && index < 4 && newsList.length > 0) {
                return InkWell(
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => NewsDetailPage(
                                  newsItem: newsList[index],
                                )));
                  },
                  child: Card(
                    elevation: 2.0,
                    margin: EdgeInsets.all(4.0),
                    child: Container(
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Hero(
                            tag: index == 1
                                ? "hero-image${newsList[1].id}"
                                : index == 2
                                    ? "hero-image${newsList[2].id}"
                                    : "hero-image${newsList[3].id}",
                            child: Container(
                              height: 90.0,
                              width: 120.0,
                              child: CachedNetworkImage(
                                imageUrl: AppText.prefixImage +
                                    newsList[index].featuredImage,
                                placeholder: (_, url) => new Image.asset(
                                  "assets/defaultImage.jpg",
                                  fit: BoxFit.cover,
                                ),
                                errorWidget: (_, url, error) => new Image.asset(
                                  "assets/defaultImage.jpg",
                                  fit: BoxFit.cover,
                                ),
                                fadeOutDuration: new Duration(seconds: 1),
                                fadeInDuration: new Duration(seconds: 2),
                                fit: BoxFit.cover,
                              ),
                            ),
                          ),
                          Flexible(
                            child: Padding(
                              padding: EdgeInsets.all(8.0),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Text(
                                    newsList[index].title,
                                    overflow: TextOverflow.ellipsis,
                                    maxLines: 2,
                                    style: TextStyle(
                                      fontSize: 18.0,
                                      color: const Color(0xFF4D4E4E),
                                    ),
                                  ),
                                  Text(
                                    newsList[index].publishOn,
                                    overflow: TextOverflow.ellipsis,
                                    maxLines: 2,
                                    style: TextStyle(
                                      color: const Color(0xFF808080),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                );
              } else if (index == 4 && newsList.length >= 6) {
                return Container(
                  height: 175.0,
                  margin: EdgeInsets.only(
                    top: 4.0,
                    bottom: 4.0,
                    left: 4.0,
                    right: 4.0,
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      Flexible(
                        flex: 1,
                        child: InkWell(
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => NewsDetailPage(
                                          newsItem: newsList[4],
                                        )));
                          },
                          child: Container(
                            margin: EdgeInsets.only(
                              right: 4.0,
                            ),
                            decoration: BoxDecoration(
                              border: Border(
                                bottom: BorderSide(
                                  color: const Color(0xFF651FFF),
                                  width: 5.0,
                                ),
                              ),
                              color: Colors.white,
                              boxShadow: [
                                BoxShadow(
                                  color: const Color(0x26000000),
                                  blurRadius: 3.0,
                                  spreadRadius: 1.0,
                                  offset: new Offset(1.0, 0.0),
                                ),
                              ],
                            ),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Hero(
                                  tag: "hero-image${newsList[4].id}",
                                  child: Container(
                                    height: 100.0,
                                    child: CachedNetworkImage(
                                      imageUrl: AppText.prefixImage +
                                          newsList[4].featuredImage,
                                      placeholder: (_, url) => new Image.asset(
                                        "assets/defaultImage.jpg",
                                        fit: BoxFit.cover,
                                      ),
                                      errorWidget: (_, url, error) =>
                                          new Image.asset(
                                        "assets/defaultImage.jpg",
                                        fit: BoxFit.cover,
                                      ),
                                      fadeOutDuration: new Duration(seconds: 1),
                                      fadeInDuration: new Duration(seconds: 2),
                                      fit: BoxFit.cover,
                                    ),
                                  ),
                                ),
                                Container(
                                  padding: EdgeInsets.all(8.0),
                                  child: Text(
                                    newsList[4].title,
                                    maxLines: 2,
                                    overflow: TextOverflow.ellipsis,
                                    style: TextStyle(
                                      color: const Color(0xFF4D4E4E),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                      Flexible(
                        flex: 1,
                        child: InkWell(
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => NewsDetailPage(
                                          newsItem: newsList[5],
                                        )));
                          },
                          child: Container(
                            margin: EdgeInsets.only(
                              left: 4.0,
                            ),
                            decoration: BoxDecoration(
                              border: Border(
                                bottom: BorderSide(
                                  color: const Color(0xFF651FFF),
                                  width: 5.0,
                                ),
                              ),
                              color: Colors.white,
                              boxShadow: [
                                BoxShadow(
                                  color: const Color(0x26000000),
                                  blurRadius: 3.0,
                                  spreadRadius: 1.0,
                                  offset: new Offset(1.0, 0.0),
                                ),
                              ],
                            ),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Hero(
                                  tag: "hero-image${newsList[5].id}",
                                  child: Container(
                                    height: 100.0,
                                    child: CachedNetworkImage(
                                      imageUrl: AppText.prefixImage +
                                          newsList[5].featuredImage,
                                      placeholder: (_, url) => new Image.asset(
                                        "assets/defaultImage.jpg",
                                        fit: BoxFit.cover,
                                      ),
                                      errorWidget: (_, url, error) =>
                                          new Image.asset(
                                        "assets/defaultImage.jpg",
                                        fit: BoxFit.cover,
                                      ),
                                      fadeOutDuration: new Duration(seconds: 1),
                                      fadeInDuration: new Duration(seconds: 2),
                                      fit: BoxFit.cover,
                                    ),
                                  ),
                                ),
                                Container(
                                  padding: EdgeInsets.all(8.0),
                                  child: Text(
                                    newsList[5].title,
                                    maxLines: 2,
                                    overflow: TextOverflow.ellipsis,
                                    style: TextStyle(
                                      color: const Color(0xFF4D4E4E),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                );
              } else if (index == 5 && newsList.length >= 8) {
                return Container(
                  height: 175.0,
                  margin: EdgeInsets.only(
                    top: 4.0,
                    bottom: 8.0,
                    left: 4.0,
                    right: 4.0,
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      Flexible(
                        flex: 1,
                        child: InkWell(
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => NewsDetailPage(
                                          newsItem: newsList[6],
                                        )));
                          },
                          child: Container(
                            margin: EdgeInsets.only(
                              right: 4.0,
                            ),
                            decoration: BoxDecoration(
                              border: Border(
                                bottom: BorderSide(
                                  color: const Color(0xFF651FFF),
                                  width: 5.0,
                                ),
                              ),
                              color: Colors.white,
                              boxShadow: [
                                BoxShadow(
                                  color: const Color(0x26000000),
                                  blurRadius: 3.0,
                                  spreadRadius: 1.0,
                                  offset: new Offset(1.0, 0.0),
                                ),
                              ],
                            ),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Hero(
                                  tag: "hero-image${newsList[6].id}",
                                  child: Container(
                                    height: 100.0,
                                    child: CachedNetworkImage(
                                      imageUrl: AppText.prefixImage +
                                          newsList[6].featuredImage,
                                      placeholder: (_, url) => new Image.asset(
                                        "assets/defaultImage.jpg",
                                        fit: BoxFit.cover,
                                      ),
                                      errorWidget: (_, url, error) =>
                                          new Image.asset(
                                        "assets/defaultImage.jpg",
                                        fit: BoxFit.cover,
                                      ),
                                      fadeOutDuration: new Duration(seconds: 1),
                                      fadeInDuration: new Duration(seconds: 2),
                                      fit: BoxFit.cover,
                                    ),
                                  ),
                                ),
                                Container(
                                  padding: EdgeInsets.all(8.0),
                                  child: Text(
                                    newsList[6].title,
                                    maxLines: 2,
                                    overflow: TextOverflow.ellipsis,
                                    style: TextStyle(
                                      color: const Color(0xFF4D4E4E),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                      Flexible(
                        flex: 1,
                        child: InkWell(
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => NewsDetailPage(
                                          newsItem: newsList[7],
                                        )));
                          },
                          child: Container(
                            margin: EdgeInsets.only(
                              left: 4.0,
                            ),
                            decoration: BoxDecoration(
                              border: Border(
                                bottom: BorderSide(
                                  color: const Color(0xFF651FFF),
                                  width: 5.0,
                                ),
                              ),
                              color: Colors.white,
                              boxShadow: [
                                BoxShadow(
                                  color: const Color(0x26000000),
                                  blurRadius: 3.0,
                                  spreadRadius: 1.0,
                                  offset: new Offset(1.0, 0.0),
                                ),
                              ],
                            ),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Hero(
                                  tag: "hero-image${newsList[7].id}",
                                  child: Container(
                                    height: 100.0,
                                    child: CachedNetworkImage(
                                      imageUrl: AppText.prefixImage +
                                          newsList[7].featuredImage,
                                      placeholder: (_, url) => new Image.asset(
                                        "assets/defaultImage.jpg",
                                        fit: BoxFit.cover,
                                      ),
                                      errorWidget: (_, url, error) =>
                                          new Image.asset(
                                        "assets/defaultImage.jpg",
                                        fit: BoxFit.cover,
                                      ),
                                      fadeOutDuration: new Duration(seconds: 1),
                                      fadeInDuration: new Duration(seconds: 2),
                                      fit: BoxFit.cover,
                                    ),
                                  ),
                                ),
                                Container(
                                  padding: EdgeInsets.all(8.0),
                                  child: Text(
                                    newsList[7].title,
                                    maxLines: 2,
                                    overflow: TextOverflow.ellipsis,
                                    style: TextStyle(
                                      color: const Color(0xFF4D4E4E),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                );
              } else if (index == 6 && newsList.length >= 9) {
                return InkWell(
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => NewsDetailPage(
                                  newsItem: newsList[8],
                                )));
                  },
                  child: Container(
                    height: 270.0,
                    margin: EdgeInsets.only(
                      bottom: 4.0,
                    ),
                    child: Stack(
                      children: <Widget>[
                        Hero(
                          tag: "hero-image${newsList[8].id}",
                          child: Container(
                            height: 270.0,
                            child: CachedNetworkImage(
                              imageUrl: AppText.prefixImage +
                                  newsList[8].featuredImage,
                              placeholder: (_, url) => new Image.asset(
                                "assets/defaultImage.jpg",
                                fit: BoxFit.cover,
                              ),
                              errorWidget: (_, url, error) => new Image.asset(
                                "assets/defaultImage.jpg",
                                fit: BoxFit.cover,
                              ),
                              fadeOutDuration: new Duration(seconds: 1),
                              fadeInDuration: new Duration(seconds: 2),
                              fit: BoxFit.cover,
                            ),
                          ),
                        ),
                        Container(
                          decoration: BoxDecoration(
                            gradient: LinearGradient(
                              begin: Alignment.topCenter,
                              end: Alignment.bottomCenter,
                              colors: [
                                const Color(0x00000000),
                                const Color(0x00000000),
                                const Color(0x00000000),
                                const Color(0x99000000),
                                //const Color(0xB3000000),
                                const Color(0xE6000000),
                                const Color(0xE6000000),
                              ],
                            ),
                          ),
                        ),
                        Positioned(
                          bottom: 0.0,
                          left: 0.0,
                          right: 0.0,
                          child: Container(
                            padding: EdgeInsets.all(8.0),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  newsList[8].title,
                                  maxLines: 2,
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 18.0,
                                  ),
                                ),
                                Row(
                                  children: <Widget>[
                                    Text(
                                      newsList[8].authorName +
                                          "   |   " +
                                          newsList[8].publishOn,
                                      style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 15.0,
                                      ),
                                    ),
                                    Flexible(
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.end,
                                        children: <Widget>[
                                          IconButton(
                                            padding: EdgeInsets.all(0.0),
                                            icon: Icon(Icons.bookmark_border),
                                            color: Colors.white,
                                            onPressed: () {},
                                          ),
                                          IconButton(
                                            padding: EdgeInsets.all(0.0),
                                            icon: Icon(Icons.share),
                                            color: Colors.white,
                                            onPressed: () {},
                                          ),
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                );
              } else if (index > 6) {
                return InkWell(
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => NewsDetailPage(
                                  newsItem: newsList[index + 2],
                                )));
                  },
                  child: Card(
                    elevation: 2.0,
                    child: Container(
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Hero(
                            tag: "hero-image${newsList[index + 2].id}",
                            child: Container(
                              height: 90.0,
                              width: 120.0,
                              child: CachedNetworkImage(
                                imageUrl: AppText.prefixImage +
                                    newsList[index + 2].featuredImage,
                                placeholder: (_, url) => new Image.asset(
                                  "assets/defaultImage.jpg",
                                  fit: BoxFit.cover,
                                ),
                                errorWidget: (_, url, error) => new Image.asset(
                                  "assets/defaultImage.jpg",
                                  fit: BoxFit.cover,
                                ),
                                fadeOutDuration: new Duration(seconds: 1),
                                fadeInDuration: new Duration(seconds: 2),
                                fit: BoxFit.cover,
                              ),
                            ),
                          ),
                          Flexible(
                            child: Padding(
                              padding: EdgeInsets.all(8.0),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Text(
                                    newsList[index + 2].title,
                                    overflow: TextOverflow.ellipsis,
                                    maxLines: 2,
                                    style: TextStyle(
                                      fontSize: 18.0,
                                      color: const Color(0xFF4D4E4E),
                                    ),
                                  ),
                                  Text(
                                    newsList[index + 2].publishOn,
                                    overflow: TextOverflow.ellipsis,
                                    maxLines: 2,
                                    style: TextStyle(
                                      color: const Color(0xFF808080),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                );
              }
            }, childCount: newsList.length - 2),
          ),
        ],
      ),
    );
  }
}
