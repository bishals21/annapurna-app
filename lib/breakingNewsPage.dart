import 'dart:async';
import 'dart:convert';

import 'package:annapurna_app/appText.dart';
import 'package:annapurna_app/apputil.dart';
import 'package:annapurna_app/model/newsItem.dart';
import 'package:annapurna_app/newsDetailPage.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class BreakingNewsPage extends StatefulWidget {
  BreakingNewsPage({Key key}) : super(key: key);
  @override
  _BreakingNewsPageState createState() => _BreakingNewsPageState();
}

class _BreakingNewsPageState extends State<BreakingNewsPage> {
  List<NewsItem> list = new List();

  Future<Null> _getBreakingNews() async {
    bool isConnected = await AppUtil.checkConnection();
    final Completer<Null> completer = new Completer<Null>();

    if (isConnected) {
      http.get(AppText.baseURl + "breaking/news").then((response) {
        print("Breaking news:${response.body}");

        if (response.statusCode == 200) {
          List<NewsItem> newsList = new List();
          var responseBody = json.decode(response.body);
          var news = responseBody['data'];
          for (int i = 0; i < news.length; i++) {
            List<NewsItem> relatedNewsList = new List();
            var relatedNews = news[i]['relatedNews'];
            relatedNews.forEach((item) {
              relatedNewsList.add(new NewsItem(
                id: item['id'],
                title: item['title'],
                introText: item['introText'] ?? "",
                featuredImage: item['featuredImage'].toString() ?? "",
                videoLink: item['videoLink'] ?? "",
                publishOn: item['publishOn'],
                authorName: item['author']['name'].toString(),
                categoryName: item['categories'].isNotEmpty
                    ? item['categories'][0]['name']
                    : "",
              ));
            });
            newsList.add(new NewsItem(
              id: news[i]['id'],
              title: news[i]['title'],
              introText: news[i]['introText'] ?? "",
              featuredImage: news[i]['featuredImage'].toString() ?? "",
              videoLink: news[i]['videoLink'] ?? "",
              publishOn: news[i]['publishOn'],
              authorName: news[i]['author']['name'].toString(),
              categoryName: news[i]['categories'].isNotEmpty
                  ? news[i]['categories'][0]['name']
                  : "",
              relatedNews: relatedNewsList,
              isBreaking: true,
            ));
          }
          if (mounted) {
            print("Reached mounted");
            this.setState(() {
              list = newsList;
              print(list.length);
              _getMainNews();
              completer.complete(null);
            });
          }
          return completer.future;
        }
      });
    } else {
      print("No internet connection");
      return completer.future;
    }
  }

  Future<Null> _getMainNews() async {
    bool isConnected = await AppUtil.checkConnection();
    final Completer<Null> completer = new Completer<Null>();
    print("status $isConnected");

    if (isConnected) {
      print("Fetching main news");
      http.get(AppText.baseURl + "main/news").then((response) {
        print("Main news:${response.body}");
        if (response.statusCode == 200) {
          List<NewsItem> newsList = new List();
          var responseBody = json.decode(response.body);
          var news = responseBody['data'];
          for (int i = 0; i < news.length; i++) {
            newsList.add(new NewsItem(
              id: news[i]['id'],
              title: news[i]['title'],
              introText: news[i]['introText'] ?? "",
              featuredImage: news[i]['featuredImage'],
              videoLink: news[i]['videoLink'] ?? "",
              publishOn: news[i]['publishOn'],
              authorName: news[i]['author']['name'],
              categoryName: news[i]['categories'].isNotEmpty
                  ? news[i]['categories'][0]['name']
                  : "",
              relatedNews: [],
              isBreaking: false,
            ));
          }
          if (mounted) {
            print("Reached Second mounted");
            this.setState(() {
              print("newslist : ${newsList.length}");
              list.addAll(newsList);
              completer.complete(null);
            });
          }
          return completer.future;
        }
      });
      print("Finished");
    } else {
      print("No internet connection");
      return completer.future;
    }
  }

  @override
  void initState() {
    _getBreakingNews();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var yOffset =
        NestedScrollView.sliverOverlapAbsorberHandleFor(context).layoutExtent;
    return list.isNotEmpty
        ? Transform.translate(
            offset: Offset(0.0, yOffset),
            child: RefreshIndicator(
              onRefresh: _getBreakingNews,
              child: Transform.translate(
                offset: Offset(0.0, -yOffset),
                child: CustomScrollView(
                  slivers: <Widget>[
                    SliverOverlapInjector(
                      handle: NestedScrollView.sliverOverlapAbsorberHandleFor(
                          context),
                    ),
                    SliverList(
                      delegate: SliverChildBuilderDelegate(
                        (context, index) {
                          return Container(
                            margin: EdgeInsets.only(
                              bottom: 8.0,
                            ),
                            child: Column(
                              mainAxisSize: MainAxisSize.min,
                              children: <Widget>[
                                InkWell(
                                  onTap: () {
                                    Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) =>
                                                NewsDetailPage(
                                                  newsItem: list[index],
                                                )));
                                  },
                                  child: Container(
                                    height: 270.0,
                                    child: Stack(
                                      children: <Widget>[
                                        Hero(
                                          tag: "hero-image${list[index].id}",
                                          child: Container(
                                            height: 270.0,
                                            child: CachedNetworkImage(
                                              imageUrl: list[index]
                                                          .featuredImage ==
                                                      null
                                                  ? ""
                                                  : AppText.prefixImage +
                                                      list[index].featuredImage,
                                              placeholder: (_, url) =>
                                                  new Image.asset(
                                                "assets/defaultImage.jpg",
                                                fit: BoxFit.cover,
                                              ),
                                              errorWidget: (_, url, error) =>
                                                  new Image.asset(
                                                "assets/defaultImage.jpg",
                                                fit: BoxFit.cover,
                                              ),
                                              fadeOutDuration:
                                                  new Duration(seconds: 1),
                                              fadeInDuration:
                                                  new Duration(seconds: 2),
                                              fit: BoxFit.cover,
                                            ),
                                          ),
                                        ),
                                        Container(
                                          decoration: BoxDecoration(
                                            gradient: LinearGradient(
                                              begin: Alignment.topCenter,
                                              end: Alignment.bottomCenter,
                                              colors: [
                                                const Color(0x00000000),
                                                const Color(0x00000000),
                                                const Color(0x00000000),
                                                const Color(0x99000000),
                                                //const Color(0xB3000000),
                                                const Color(0xE6000000),
                                                const Color(0xE6000000),
                                              ],
                                            ),
                                          ),
                                        ),
                                        Positioned(
                                          bottom: 0.0,
                                          left: 0.0,
                                          right: 0.0,
                                          child: Container(
                                            padding: EdgeInsets.all(8.0),
                                            child: Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: <Widget>[
                                                list[index]
                                                        .categoryName
                                                        .isNotEmpty
                                                    ? Container(
                                                        color: Colors.red,
                                                        padding:
                                                            EdgeInsets.only(
                                                          top: 4.0,
                                                          left: 4.0,
                                                          right: 4.0,
                                                        ),
                                                        child: Text(
                                                          list[index]
                                                              .categoryName,
                                                          style: TextStyle(
                                                            color: Colors.white,
                                                            fontSize: 15.0,
                                                          ),
                                                        ),
                                                      )
                                                    : Container(),
                                                Padding(
                                                  padding: EdgeInsets.only(
                                                    top: 3.0,
                                                  ),
                                                ),
                                                Text(
                                                  list[index].title,
                                                  maxLines: 2,
                                                  style: TextStyle(
                                                    color: Colors.white,
                                                    fontSize: 18.0,
                                                  ),
                                                ),
                                                Row(
                                                  children: <Widget>[
                                                    Text(
                                                      list[index].authorName +
                                                          "   |   " +
                                                          list[index].publishOn,
                                                      style: TextStyle(
                                                        color: Colors.white,
                                                        fontSize: 15.0,
                                                      ),
                                                    ),
                                                    Flexible(
                                                      child: Row(
                                                        mainAxisAlignment:
                                                            MainAxisAlignment
                                                                .end,
                                                        children: <Widget>[
                                                          IconButton(
                                                            padding:
                                                                EdgeInsets.all(
                                                                    0.0),
                                                            icon: Icon(Icons
                                                                .bookmark_border),
                                                            color: Colors.white,
                                                            onPressed: () {},
                                                          ),
                                                          IconButton(
                                                            padding:
                                                                EdgeInsets.all(
                                                                    0.0),
                                                            icon: Icon(
                                                                Icons.share),
                                                            color: Colors.white,
                                                            onPressed: () {},
                                                          ),
                                                        ],
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ],
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                                list[index].relatedNews.isNotEmpty &&
                                        list[index].isBreaking
                                    ? Container(
                                        margin: EdgeInsets.all(16.0),
                                        decoration: BoxDecoration(
                                          border: Border(
                                            top: BorderSide(
                                              color: Colors.grey[300],
                                            ),
                                            bottom: BorderSide(
                                              color: Colors.grey[300],
                                            ),
                                          ),
                                        ),
                                        child: Column(
                                          children: list[index]
                                              .relatedNews
                                              .map((item) {
                                            return InkWell(
                                              onTap: () {
                                                Navigator.push(
                                                    context,
                                                    MaterialPageRoute(
                                                        builder: (context) =>
                                                            NewsDetailPage(
                                                              newsItem: item,
                                                            )));
                                              },
                                              child: Container(
                                                padding: EdgeInsets.all(8.0),
                                                child: Row(
                                                  children: <Widget>[
                                                    ImageIcon(
                                                      AssetImage(
                                                          "assets/listIcon.png"),
                                                      color: Colors.blue,
                                                    ),
                                                    Padding(
                                                      padding:
                                                          EdgeInsets.symmetric(
                                                              horizontal: 8.0),
                                                    ),
                                                    Flexible(
                                                      child: Text(
                                                        item.title,
                                                        maxLines: 2,
                                                        style: TextStyle(
                                                          color: Colors.black,
                                                          fontSize: 16.0,
                                                        ),
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            );
                                          }).toList(),
                                        ),
                                      )
                                    : Container()
                              ],
                            ),
                          );
                        },
                        childCount: list.length,
                      ),
                    ),
                  ],
                ),
              ),
            ),
          )
        : Container();
  }
}
