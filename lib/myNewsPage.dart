import 'dart:async';
import 'dart:convert';

import 'package:annapurna_app/appText.dart';
import 'package:annapurna_app/apputil.dart';
import 'package:annapurna_app/chooseCategoryPage.dart';
import 'package:annapurna_app/model/categoryItem.dart';
import 'package:annapurna_app/model/newsItem.dart';
import 'package:annapurna_app/newsDetailPage.dart';
import 'package:annapurna_app/stateContainer/stateContainer.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class MyNewsPage extends StatefulWidget {
  MyNewsPage({Key key}) : super(key: key);
  @override
  _MyNewsPageState createState() => _MyNewsPageState();
}

class _MyNewsPageState extends State<MyNewsPage> {
  List<CategoryItem> myCategoryList = new List();
  List<NewsItem> myNewsList = new List();
  List<Color> colorList = [
    const Color(0xFFEF5350),
    const Color(0xFFEC407A),
    const Color(0xFFAB47BC),
    const Color(0xFF7E57C2),
    const Color(0xFF5C6BC0),
    const Color(0xFF42A5F5),
    const Color(0xFF29B6FC),
    const Color(0xFF26C6DA),
    const Color(0xFF26A69A),
    const Color(0xFF66BB6A),
    const Color(0xFF9CCC65),
    const Color(0xFFD4E157),
    const Color(0xFFFFEE58),
    const Color(0xFFFFCA28),
    const Color(0xFFFFA726),
    const Color(0xFFFF7043),
    const Color(0xFF8D6E63),
    const Color(0xFFBDBDBD),
    const Color(0xFFEF5350),
    const Color(0xFFEC407A),
  ];

  Future<Null> _getMyCategoryList() {
    myCategoryList.clear();
    var container = StateContainer.of(context);
    List<CategoryItem> categoryList = container.categoryList;

    categoryList.forEach((item) {
      if (item.choose == true) {
        myCategoryList.add(item);
        _getNews(item.alias);
      }
    });
  }

  _getNews(String alias) async {
    bool isConnected = await AppUtil.checkConnection();

    print(isConnected);
    if (isConnected) {
      http
          .get(
              "http://bg.annapurnapost.com/api/news/list?_format=json&per_page=4&category_alias=" +
                  alias)
          .then((response) {
        print("Response Body:${response.body}");
        if (response.statusCode == 200) {
          List<NewsItem> newsItemList = new List();
          var responseBody = json.decode(response.body);
          if (responseBody['status'] == "success") {
            var news = responseBody['data'];
            for (int i = 0; i < news.length; i++) {
              newsItemList.add(new NewsItem(
                id: news[i]['id'],
                title: news[i]['title'],
                introText: news[i]['introText'],
                featuredImage: news[i]['featuredImage'],
                videoLink: news[i]['videoLink'],
                publishOn: news[i]['publishOn'],
                authorName: news[i]['author']['name'].toString(),
                categoryName: news[i]['categories'].isNotEmpty
                    ? news[i]['categories'][0]['name']
                    : "",
                alias: alias,
              ));
            }

            setState(() {
              myNewsList.addAll(newsItemList);
            });
          }
        }
      });
    } else {
      print("No internet connection");
    }
  }

  @override
  void initState() {
    new Future.delayed(Duration.zero, () {
      _getMyCategoryList();
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // var yOffset =
    //     NestedScrollView.sliverOverlapAbsorberHandleFor(context).layoutExtent;

    _navigateToAddCategory() async {
      bool isChanged = await Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => ChooseCategoryPage(),
              fullscreenDialog: true));

      if (isChanged) {
        myNewsList.clear();
        _getMyCategoryList();
      }
    }

    return Stack(
      children: <Widget>[
        CustomScrollView(
          slivers: <Widget>[
            SliverOverlapInjector(
              handle: NestedScrollView.sliverOverlapAbsorberHandleFor(context),
            ),
            SliverList(
              delegate: SliverChildBuilderDelegate(
                (context, index) {
                  List<NewsItem> newsListFromALias = new List();
                  myNewsList.forEach((item) {
                    if (item.alias == myCategoryList[index].alias) {
                      newsListFromALias.add(item);
                    }
                  });

                  return Container(
                    margin: EdgeInsets.only(top: 8.0, bottom: 8.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Container(
                          padding: EdgeInsets.all(8.0),
                          child: Row(
                            children: <Widget>[
                              Container(
                                height: 15.0,
                                width: 15.0,
                                decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                  color: colorList[index],
                                  //F50057
                                ),
                              ),
                              Padding(
                                padding: EdgeInsets.only(right: 8.0),
                              ),
                              Text(
                                myCategoryList[index].name,
                                style: Theme.of(context).textTheme.title,
                              ),
                              Padding(
                                padding: EdgeInsets.only(right: 8.0),
                              ),
                              Flexible(
                                child: Divider(
                                  color: Colors.grey,
                                  height: 5.0,
                                ),
                              ),
                            ],
                          ),
                        ),
                        newsListFromALias.isNotEmpty
                            ? ListView.builder(
                                padding: EdgeInsets.only(top: 8.0),
                                itemCount: newsListFromALias.length,
                                itemBuilder: (context, newsIndex) {
                                  if (newsIndex == 0) {
                                    return InkWell(
                                      onTap: () {
                                        Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                                builder: (context) =>
                                                    NewsDetailPage(
                                                      newsItem:
                                                          newsListFromALias[
                                                              newsIndex],
                                                    )));
                                      },
                                      child: Container(
                                        height: 270.0,
                                        margin: EdgeInsets.only(
                                          bottom: 4.0,
                                        ),
                                        child: Stack(
                                          children: <Widget>[
                                            Hero(
                                              tag:
                                                  "hero-image${newsListFromALias[newsIndex].id}",
                                              child: Container(
                                                height: 270.0,
                                                child: CachedNetworkImage(
                                                  imageUrl:
                                                      AppText.prefixImage +
                                                          newsListFromALias[
                                                                  newsIndex]
                                                              .featuredImage,
                                                  placeholder: (_, url) =>
                                                      new Image.asset(
                                                    "assets/defaultImage.jpg",
                                                    fit: BoxFit.cover,
                                                  ),
                                                  errorWidget:
                                                      (_, url, error) =>
                                                          new Image.asset(
                                                    "assets/defaultImage.jpg",
                                                    fit: BoxFit.cover,
                                                  ),
                                                  fadeOutDuration:
                                                      new Duration(seconds: 1),
                                                  fadeInDuration:
                                                      new Duration(seconds: 2),
                                                  fit: BoxFit.cover,
                                                ),
                                              ),
                                            ),
                                            Container(
                                              decoration: BoxDecoration(
                                                gradient: LinearGradient(
                                                  begin: Alignment.topCenter,
                                                  end: Alignment.bottomCenter,
                                                  colors: [
                                                    const Color(0x00000000),
                                                    const Color(0x00000000),
                                                    const Color(0x00000000),
                                                    const Color(0x99000000),
                                                    const Color(0xE6000000),
                                                    const Color(0xE6000000),
                                                  ],
                                                ),
                                              ),
                                            ),
                                            Positioned(
                                              bottom: 0.0,
                                              left: 0.0,
                                              right: 0.0,
                                              child: Container(
                                                padding: EdgeInsets.all(8.0),
                                                child: Column(
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.start,
                                                  children: <Widget>[
                                                    Text(
                                                      newsListFromALias[
                                                              newsIndex]
                                                          .title,
                                                      maxLines: 2,
                                                      style: TextStyle(
                                                        color: Colors.white,
                                                        fontSize: 18.0,
                                                      ),
                                                    ),
                                                    Row(
                                                      children: <Widget>[
                                                        Text(
                                                          newsListFromALias[
                                                                          newsIndex]
                                                                      .authorName ==
                                                                  "null"
                                                              ? newsListFromALias[
                                                                      newsIndex]
                                                                  .publishOn
                                                              : newsListFromALias[
                                                                          newsIndex]
                                                                      .authorName +
                                                                  "   |   " +
                                                                  newsListFromALias[
                                                                          newsIndex]
                                                                      .publishOn,
                                                          style: TextStyle(
                                                            color: Colors.white,
                                                            fontSize: 15.0,
                                                          ),
                                                        ),
                                                        Flexible(
                                                          child: Row(
                                                            mainAxisAlignment:
                                                                MainAxisAlignment
                                                                    .end,
                                                            children: <Widget>[
                                                              IconButton(
                                                                padding:
                                                                    EdgeInsets
                                                                        .all(
                                                                            0.0),
                                                                icon: Icon(Icons
                                                                    .bookmark_border),
                                                                color: Colors
                                                                    .white,
                                                                onPressed:
                                                                    () {},
                                                              ),
                                                              IconButton(
                                                                padding:
                                                                    EdgeInsets
                                                                        .all(
                                                                            0.0),
                                                                icon: Icon(Icons
                                                                    .share),
                                                                color: Colors
                                                                    .white,
                                                                onPressed:
                                                                    () {},
                                                              ),
                                                            ],
                                                          ),
                                                        ),
                                                      ],
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    );
                                  } else {
                                    return InkWell(
                                      onTap: () {
                                        Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                                builder: (context) =>
                                                    NewsDetailPage(
                                                      newsItem:
                                                          newsListFromALias[
                                                              newsIndex],
                                                    )));
                                      },
                                      child: Card(
                                        elevation: 2.0,
                                        margin: EdgeInsets.all(4.0),
                                        child: Container(
                                          child: Row(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.center,
                                            children: <Widget>[
                                              Hero(
                                                tag: newsIndex == 1
                                                    ? "hero-image${newsListFromALias[1].id}"
                                                    : newsIndex == 2
                                                        ? "hero-image${newsListFromALias[2].id}"
                                                        : "hero-image${newsListFromALias[3].id}",
                                                child: Container(
                                                  height: 90.0,
                                                  width: 120.0,
                                                  child: CachedNetworkImage(
                                                    imageUrl:
                                                        AppText.prefixImage +
                                                            newsListFromALias[
                                                                    newsIndex]
                                                                .featuredImage,
                                                    placeholder: (_, url) =>
                                                        new Image.asset(
                                                      "assets/defaultImage.jpg",
                                                      fit: BoxFit.cover,
                                                    ),
                                                    errorWidget:
                                                        (_, url, error) =>
                                                            new Image.asset(
                                                      "assets/defaultImage.jpg",
                                                      fit: BoxFit.cover,
                                                    ),
                                                    fadeOutDuration:
                                                        new Duration(
                                                            seconds: 1),
                                                    fadeInDuration:
                                                        new Duration(
                                                            seconds: 2),
                                                    fit: BoxFit.cover,
                                                  ),
                                                ),
                                              ),
                                              Flexible(
                                                child: Padding(
                                                  padding: EdgeInsets.all(8.0),
                                                  child: Column(
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment
                                                            .start,
                                                    children: <Widget>[
                                                      Text(
                                                        newsListFromALias[
                                                                newsIndex]
                                                            .title,
                                                        overflow: TextOverflow
                                                            .ellipsis,
                                                        maxLines: 2,
                                                        style: TextStyle(
                                                          fontSize: 16.0,
                                                          color: const Color(
                                                              0xFF4D4E4E),
                                                        ),
                                                      ),
                                                      Text(
                                                        newsListFromALias[
                                                                newsIndex]
                                                            .publishOn,
                                                        overflow: TextOverflow
                                                            .ellipsis,
                                                        maxLines: 2,
                                                        style: TextStyle(
                                                          color: const Color(
                                                              0xFF808080),
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                    );
                                  }
                                },
                                shrinkWrap: true,
                                physics: new ScrollPhysics(
                                    parent: NeverScrollableScrollPhysics()),
                              )
                            : Container(),
                      ],
                    ),
                  );
                },
                childCount: myCategoryList.length,
              ),
            ),
          ],
        ),
        Positioned(
          bottom: 16.0,
          right: 16.0,
          child: FloatingActionButton(
            onPressed: _navigateToAddCategory,
            child: Icon(Icons.edit),
          ),
        ),
      ],
    );
  }
}
