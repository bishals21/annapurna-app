import 'package:annapurna_app/annapurnaExpressHomePage.dart';
import 'package:annapurna_app/breakingNewsPage.dart';
import 'package:annapurna_app/latestNewsPage.dart';
import 'package:annapurna_app/myNewsPage.dart';
import 'package:annapurna_app/trendingNewsPage.dart';
import 'package:flutter/material.dart';

class Homepage extends StatefulWidget {
  const Homepage({Key key}) : super(key: key);
  @override
  _HomepageState createState() => _HomepageState();
}

class _HomepageState extends State<Homepage> {
  GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey();
  final Key breakingKey = PageStorageKey('breakingKey');
  final Key latestKey = PageStorageKey('latestKey');
  final Key trendingKey = PageStorageKey('trendingKey');
  final Key myNewsKey = PageStorageKey('myNewsKey');

  BreakingNewsPage breakingNewsPage;
  LatestNewsPage latestNewsPage;
  TrendingNewsPage trendingNewsPage;
  MyNewsPage myNewsPage;

  List<Widget> _children = [];

  int _currentIndex = 0;

  bool _innerListIsScrolled = false;
  ScrollController scrollController = new ScrollController();

  void _updateScrollPosition() {
    print(scrollController.position.extentBefore);
    if (!_innerListIsScrolled &&
        scrollController.position.extentBefore > 80.0) {
      setState(() {
        _innerListIsScrolled = true;
      });
    } else if (_innerListIsScrolled &&
        scrollController.position.extentBefore < 80.0) {
      setState(() {
        _innerListIsScrolled = false;
        // Reset scroll positions of the TabBarView pages
      });
    }
  }

  @override
  void initState() {
    breakingNewsPage = BreakingNewsPage(
      key: breakingKey,
    );
    latestNewsPage = LatestNewsPage(
      key: latestKey,
    );
    trendingNewsPage = TrendingNewsPage(
      key: trendingKey,
    );
    myNewsPage = MyNewsPage(
      key: myNewsKey,
    );

    _children = [
      breakingNewsPage,
      latestNewsPage,
      trendingNewsPage,
      myNewsPage
    ];

    scrollController.addListener(_updateScrollPosition);
    super.initState();
  }

  @override
  void dispose() {
    scrollController.removeListener(_updateScrollPosition);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final BottomNavigationBar botNavBar = BottomNavigationBar(
      items: [
        BottomNavigationBarItem(
          icon: new Icon(Icons.home),
          title: new Text('मुख्य समाचार'),
        ),
        BottomNavigationBarItem(
          icon: new Icon(Icons.next_week),
          title: new Text('समाचार'),
        ),
        BottomNavigationBarItem(
          icon: new Icon(Icons.trending_up),
          title: new Text('लोकप्रिय'),
        ),
        BottomNavigationBarItem(
          icon: new Icon(Icons.subscriptions),
          title: new Text('मेरो रुची'),
        ),
      ],
      currentIndex: _currentIndex,
      type: BottomNavigationBarType.fixed,
      onTap: (int index) {
        setState(() {
          _currentIndex = index;
        });
      },
    );
    return Scaffold(
      key: _scaffoldKey,
      body: SafeArea(
        child: NestedScrollView(
          controller: scrollController,
          headerSliverBuilder: (context, innerBoxIsScrolled) {
            return <Widget>[
              SliverOverlapAbsorber(
                handle:
                    NestedScrollView.sliverOverlapAbsorberHandleFor(context),
                child: new SliverAppBar(
                  expandedHeight: 150.0,
                  backgroundColor: Colors.white,
                  elevation: 0.0,
                  pinned: true,
                  //   leading: IconButton(
                  //     icon: Icon(Icons.menu),
                  //     color: Colors.grey,
                  //     onPressed: () {
                  //       Navigator.push(
                  //           context,
                  //           MaterialPageRoute(
                  //             builder: (context) {
                  //               return MenuPage();
                  //             },
                  //             fullscreenDialog: true,
                  //           ));
                  //     },
                  //   ),
                  //floating: true,
                  title: _innerListIsScrolled
                      ? Image.asset(
                          "assets/annapurnaLogo.png",
                          width: 100.0,
                        )
                      : Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Image.asset(
                              "assets/annapurnaLogo.png",
                              width: 100.0,
                            ),
                            Padding(
                              padding: EdgeInsets.symmetric(
                                horizontal: 16.0,
                              ),
                            ),
                            InkWell(
                              onTap: () {
                                Navigator.push(context,
                                    MaterialPageRoute(builder: (context) {
                                  return AnnapurnaExpressHomePage();
                                }));
                              },
                              child: Image.asset(
                                "assets/expressLogo.png",
                                width: 100.0,
                              ),
                            ),
                          ],
                        ),
                  centerTitle: true,
                  flexibleSpace: new FlexibleSpaceBar(
                    background: Column(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[
                        Container(
                          height: 80.0,
                          child: Image(
                            image: AssetImage("assets/bellLogo.png"),
                          ),
                        ),
                        Text(
                          "असोज १२ २०७५, शुक्रवार",
                        ),
                      ],
                    ),
                    collapseMode: CollapseMode.parallax,
                  ),
                ),
              ),
            ];
          },
          body: _children[_currentIndex],
        ),
      ),
      //   bottomNavigationBar: BottomAppBar(
      //     notchMargin: 4.0,
      //     color: Colors.white,
      //     shape: CircularNotchedRectangle(),
      //     child: Container(
      //       padding: EdgeInsets.only(
      //         left: 8.0,
      //         right: 8.0,
      //         top: 2.0,
      //         bottom: 2.0,
      //       ),
      //       child: Row(
      //         mainAxisSize: MainAxisSize.max,
      //         mainAxisAlignment: MainAxisAlignment.spaceBetween,
      //         children: <Widget>[
      //           InkWell(
      //             onTap: () {
      //               onTabTapped(0);
      //             },
      //             child: Column(
      //               mainAxisSize: MainAxisSize.min,
      //               children: <Widget>[
      //                 Icon(
      //                   Icons.home,
      //                   size: 30.0,
      //                   color: Colors.blue,
      //                 ),
      //                 Text(
      //                   "मुख्य",
      //                   style: TextStyle(
      //                     color: Colors.blue,
      //                   ),
      //                 ),
      //               ],
      //             ),
      //           ),
      //           InkWell(
      //             onTap: () {
      //               onTabTapped(1);
      //             },
      //             child: Column(
      //               mainAxisSize: MainAxisSize.min,
      //               children: <Widget>[
      //                 Icon(
      //                   Icons.new_releases,
      //                   size: 30.0,
      //                   color: Colors.blue,
      //                 ),
      //                 Text(
      //                   "समाचार",
      //                   style: TextStyle(
      //                     color: Colors.blue,
      //                   ),
      //                 ),
      //               ],
      //             ),
      //           ),
      //           InkWell(
      //             onTap: () {
      //               onTabTapped(2);
      //             },
      //             child: Column(
      //               mainAxisSize: MainAxisSize.min,
      //               mainAxisAlignment: MainAxisAlignment.center,
      //               children: <Widget>[
      //                 Icon(
      //                   Icons.trending_up,
      //                   size: 30.0,
      //                   color: Colors.blue,
      //                 ),
      //                 Text(
      //                   "लोकप्रिय",
      //                   style: TextStyle(
      //                     color: Colors.blue,
      //                   ),
      //                 ),
      //               ],
      //             ),
      //           ),
      //           InkWell(
      //             onTap: () {
      //               onTabTapped(3);
      //             },
      //             child: Column(
      //               mainAxisSize: MainAxisSize.min,
      //               mainAxisAlignment: MainAxisAlignment.center,
      //               children: <Widget>[
      //                 Icon(
      //                   Icons.book,
      //                   size: 30.0,
      //                   color: Colors.blue,
      //                 ),
      //                 Text(
      //                   "मेरो रुची",
      //                   style: TextStyle(
      //                     color: Colors.blue,
      //                   ),
      //                 ),
      //               ],
      //             ),
      //           ),
      //           InkWell(
      //             onTap: () {
      //               //onTabTapped(3);
      //             },
      //             child: Column(
      //               mainAxisSize: MainAxisSize.min,
      //               mainAxisAlignment: MainAxisAlignment.center,
      //               children: <Widget>[
      //                 Icon(
      //                   Icons.book,
      //                   size: 30.0,
      //                   color: Colors.blue,
      //                 ),
      //                 Text(
      //                   "रुची",
      //                   style: TextStyle(
      //                     color: Colors.blue,
      //                   ),
      //                 ),
      //               ],
      //             ),
      //           ),
      //         ],
      //       ),
      //     ),
      //   ),

      bottomNavigationBar: botNavBar,
      //   floatingActionButton: FloatingActionButton(
      //     onPressed: () {},
      //     elevation: 4.0,
      //     backgroundColor: Colors.white,
      //     child: Icon(
      //       Icons.add,
      //       color: Colors.purple,
      //     ),
      //   ),
      //   floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
    );
  }
}
