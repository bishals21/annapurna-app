import 'dart:async';
import 'dart:convert';

import 'package:annapurna_app/appText.dart';
import 'package:annapurna_app/apputil.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class HoroscopePage extends StatefulWidget {
  @override
  _HoroscopePageState createState() => _HoroscopePageState();
}

class _HoroscopePageState extends State<HoroscopePage> {
  List<String> list = new List();

  List<String> imageList = [
    "assets/brischik.png",
    "assets/brish.png",
    "assets/dhanu.png",
    "assets/kanaya.png",
    "assets/karkat.png",
    "assets/khumbha.png",
    "assets/makar.png",
    "assets/mesh.png",
    "assets/min.png",
    "assets/mithun.png",
    "assets/shingha.png",
    "assets/tula.png",
  ];
  List<String> horoscopeList = [
    "मेष",
    "बृष",
    "मिथुन",
    "कर्कट",
    "सिंह",
    "कन्या",
    "तुला",
    "बृश्चिक",
    "धनु",
    "मकर",
    "कुम्भ",
    "मीन"
  ];

  List<String> extraList = [
    "(चु चे चो ला लि लु ले लो अ)",
    "(इ उ ए ओ वा वी वु वे वो)",
    "(का कि कु घ ङ छ के को हा)",
    "(ही हु हे हो डा डि डु डे डो)",
    "(मा मि मू मे मो टा टी टु टे)",
    "(टो प पी पु ष ण ठ पे मो)",
    "(र री रु रे रो ता ती तू ते)",
    "(तो ना नी नू ने नो या यि यु)",
    "(ये यो भ भि भु ध फा ढ भे)",
    "(भो ज जि खि खु खे खो गा गि )",
    "(गू गे गो सा सी सु से सो द)",
    "(दि दू थ भ ञ दे दो चा ची)",
  ];

  Future<Null> _getHoroscope() async {
    bool isConnected = await AppUtil.checkConnection();
    final Completer<Null> completer = new Completer<Null>();
    print(isConnected);
    if (isConnected) {
      http.get(AppText.baseURl + "horoscope?_format=json").then((response) {
        print("Response Body:${response.body}");
        if (response.statusCode == 200) {
          List<String> stringList = new List();
          var responseBody = json.decode(response.body);
          var horoscope = responseBody['data'];
          stringList.add(horoscope['aries']);
          stringList.add(horoscope['taurus']);
          stringList.add(horoscope['gemini']);
          stringList.add(horoscope['cancer']);
          stringList.add(horoscope['leo']);
          stringList.add(horoscope['virgo']);
          stringList.add(horoscope['libra']);
          stringList.add(horoscope['scorpio']);
          stringList.add(horoscope['sagittarius']);
          stringList.add(horoscope['capricorn']);
          stringList.add(horoscope['aquarius']);
          stringList.add(horoscope['pisces']);
          if (mounted) {
            this.setState(() {
              list = stringList;
              completer.complete(null);
            });
          }
          return completer.future;
        }
      });
    } else {
      print("No internet connection");
      return completer.future;
    }
  }

  @override
  void initState() {
    _getHoroscope();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("राशिफल"),
      ),
      body: ListView.builder(
        itemBuilder: (context, index) {
          return Card(
            child: Container(
              padding: EdgeInsets.all(8.0),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Image.asset(
                    imageList[index],
                    height: 65.0,
                    width: 65.0,
                  ),
                  Flexible(
                    child: Container(
                      padding: EdgeInsets.all(8.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          RichText(
                            text: TextSpan(children: [
                              TextSpan(
                                text: horoscopeList[index] + " ",
                                style: TextStyle(
                                  color: Colors.black,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              TextSpan(
                                text: extraList[index],
                                style: TextStyle(
                                  color: Colors.black,
                                ),
                              ),
                            ]),
                          ),
                          Divider(
                            color: Colors.grey,
                          ),
                          Text(list[index]),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          );
        },
        itemCount: list.length,
      ),
    );
  }
}
