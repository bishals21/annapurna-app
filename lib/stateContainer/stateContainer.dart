import 'dart:async';
import 'dart:convert';

import 'package:annapurna_app/model/categoryItem.dart';
import 'package:annapurna_app/model/menuItem.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:shared_preferences/shared_preferences.dart';

class StateContainer extends StatefulWidget {
  final Widget child;
  final List<CategoryItem> categoryList;
  final List<MenuItem> menuList;

  StateContainer({
    @required this.child,
    this.categoryList,
    this.menuList,
  });

  static StateContainerState of(BuildContext context) {
    return (context.inheritFromWidgetOfExactType(_InheritedStateContainer)
            as _InheritedStateContainer)
        .data;
  }

  @override
  StateContainerState createState() => new StateContainerState();
}

class StateContainerState extends State<StateContainer> {
  Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
  List<CategoryItem> categoryList = new List();
  List<MenuItem> menuList = [
    new MenuItem(title: "मुख्य समाचार", alias: "breaking"),
    new MenuItem(title: "समाचार", alias: "latest"),
    new MenuItem(title: "लोकप्रिय", alias: "trending"),
    new MenuItem(title: "मेरो रुची", alias: "mero"),
  ];

  void updateCategoryList(List<CategoryItem> categoryLists) {
    setState(() {
      categoryList = categoryLists;
      changeCategoryList(categoryLists);
      updateMenuList(categoryLists);
    });
  }

  void updateMenuList(List<CategoryItem> categoryLists) {
    List<MenuItem> updatedList = new List();
    categoryLists.forEach((item) {
      if (item.choose) {
        updatedList.add(new MenuItem(
          alias: item.alias,
          title: item.name,
        ));
      }
    });
    setState(() {
      menuList.removeRange(4, menuList.length);
      print(menuList.last.title);
      menuList.addAll(updatedList);
      print(menuList.last.title);
    });
  }

  changeCategoryList(List<CategoryItem> categoryLists) async {
    final SharedPreferences prefs = await _prefs;

    bool isSuccess = await prefs.setString('#ANC', json.encode(categoryLists));

    setState(() {
      if (isSuccess) {
        print("Success added category");
      }
    });
  }

//   changeMenuList(List<MenuItem> menuLists) async {
//     final SharedPreferences prefs = await _prefs;

//     bool isSuccess = await prefs.setString('#ANM', json.encode(menuLists));

//     setState(() {
//       if (isSuccess) {
//         print("Success added menulist");
//       }
//     });
//   }

  @override
  Widget build(BuildContext context) {
    return new _InheritedStateContainer(
      data: this,
      child: widget.child,
    );
  }
}

class _InheritedStateContainer extends InheritedWidget {
  final StateContainerState data;

  _InheritedStateContainer({
    Key key,
    @required this.data,
    @required Widget child,
  }) : super(key: key, child: child);

  @override
  bool updateShouldNotify(_InheritedStateContainer old) {
    return true;
  }
}
