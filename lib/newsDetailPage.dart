import 'dart:async';
import 'dart:convert';

import 'package:annapurna_app/appText.dart';
import 'package:annapurna_app/apputil.dart';
import 'package:annapurna_app/model/newsItem.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:html2md/html2md.dart' as html2md;
import 'package:flutter_markdown/flutter_markdown.dart';

class NewsDetailPage extends StatefulWidget {
  final NewsItem newsItem;
  NewsDetailPage({
    Key key,
    this.newsItem,
  }) : super(key: key);

  @override
  _NewsDetailPageState createState() => _NewsDetailPageState();
}

class _NewsDetailPageState extends State<NewsDetailPage> {
  NewsItem newsItem;
  List<NewsItem> relatedNews = new List();
  String description = "";
  String pageTitle = "";

  Future<Null> _getNewsDetail() async {
    bool isConnected = await AppUtil.checkConnection();
    final Completer<Null> completer = new Completer<Null>();
    print(isConnected);
    if (isConnected) {
      http
          .get(AppText.baseURl +
              "news/" +
              newsItem.id.toString() +
              "?_format=json")
          .then((response) {
        print("Response Body:${response.body}");
        if (response.statusCode == 200) {
          var responseBody = json.decode(response.body);
          if (mounted) {
            this.setState(() {
              description = html2md.convert(responseBody['news']['content']);
              if (responseBody['categories'].isNotEmpty) {
                _getRelatedNews(responseBody['categories'][0]['alias']);
              }

              completer.complete(null);
            });
          }

          return completer.future;
        }
      });
    } else {
      print("No internet connection");
      return completer.future;
    }
  }

  Future<Null> _getRelatedNews(String alias) async {
    bool isConnected = await AppUtil.checkConnection();
    final Completer<Null> completer = new Completer<Null>();
    print(isConnected);

    if (isConnected) {
      http
          .get(
        AppText.baseURl +
            "news/list?_format=json&per_page=6&category_alias=" +
            alias,
      )
          .then((response) {
        print("Response Body:${response.body}");
        if (response.statusCode == 200) {
          List<NewsItem> newsList = new List();
          var responseBody = json.decode(response.body);
          var news = responseBody['data'];
          for (int i = 0; i < news.length; i++) {
            newsList.add(new NewsItem(
              id: news[i]['id'],
              title: news[i]['title'],
              introText: news[i]['introText'],
              featuredImage: news[i]['featuredImage'],
              videoLink: news[i]['videoLink'],
              publishOn: news[i]['publishOn'],
              authorName: news[i]['author']['name'].toString(),
              categoryName: news[i]['categories'].isNotEmpty
                  ? news[i]['categories'][0]['name']
                  : "",
            ));
          }
          if (mounted) {
            this.setState(() {
              relatedNews = newsList;
              completer.complete(null);
            });
          }
          return completer.future;
        }
      });
    } else {
      print("No internet connection");
      return completer.future;
    }
  }

  @override
  void initState() {
    newsItem = widget.newsItem;
    _getNewsDetail();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    bool _handleScrollNotification(ScrollNotification notification) {
      double visibleStatsHeight = notification.metrics.pixels;
      double screenHeight = MediaQuery.of(context).size.height -
          MediaQuery.of(context).padding.top;
      double visiblePercentage = visibleStatsHeight / screenHeight;
      pageTitle = visiblePercentage > 0.3 ? newsItem.title : "";
      setState(() {});
      return false;
    }

    return Scaffold(
      backgroundColor: Colors.white,
      body: NotificationListener<ScrollNotification>(
        onNotification: _handleScrollNotification,
        child: CustomScrollView(
          slivers: <Widget>[
            SliverAppBar(
              expandedHeight: 270.0,
              pinned: true,
              title: Text(
                pageTitle,
                maxLines: 1,
                style: TextStyle(
                  color: Colors.white,
                ),
              ),
              actions: <Widget>[
                IconButton(
                  icon: Icon(Icons.bookmark_border),
                  onPressed: () {},
                ),
                IconButton(
                  icon: Icon(Icons.share),
                  onPressed: () {},
                ),
              ],
              flexibleSpace: FlexibleSpaceBar(
                background: Stack(
                  fit: StackFit.expand,
                  children: <Widget>[
                    Container(
                      child: Hero(
                        tag: "hero-image${newsItem.id}",
                        child: Container(
                          height: 270.0,
                          child: CachedNetworkImage(
                            imageUrl:
                                AppText.prefixImage + newsItem.featuredImage,
                            placeholder: (_, url) => new Image.asset(
                              "assets/defaultImage.jpg",
                              fit: BoxFit.cover,
                            ),
                            errorWidget: (_, url, error) => new Image.asset(
                              "assets/defaultImage.jpg",
                              fit: BoxFit.cover,
                            ),
                            fadeOutDuration: new Duration(seconds: 1),
                            fadeInDuration: new Duration(seconds: 2),
                            fit: BoxFit.cover,
                          ),
                        ),
                      ),
                    ),
                    Container(
                      color: Colors.black12,
                    ),
                  ],
                ),
              ),
            ),
            SliverPadding(
              padding: EdgeInsets.symmetric(horizontal: 8.0, vertical: 8.0),
              sliver: SliverList(
                  delegate: SliverChildListDelegate([
                Text(
                  newsItem.authorName == "null"
                      ? newsItem.publishOn
                      : newsItem.authorName + "   |   " + newsItem.publishOn,
                  style: TextStyle(
                    color: const Color(0xFF4D4E4E),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(
                    top: 8.0,
                  ),
                ),
                Text(
                  newsItem.title,
                  style: TextStyle(
                    fontSize: 22.0,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(
                    top: 16.0,
                  ),
                ),
                MarkdownBody(
                  data: description,
                  styleSheet:
                      MarkdownStyleSheet.largeFromTheme(Theme.of(context))
                          .copyWith(
                    horizontalRuleDecoration: new BoxDecoration(
                      border: new Border(
                        top: new BorderSide(
                            width: 2.0, color: Colors.grey.shade300),
                      ),
                    ),
                    //blockSpacing: 16.0,
                  ),
                ),
              ])),
            ),
            relatedNews.isNotEmpty
                ? SliverToBoxAdapter(
                    child: Container(
                      padding: EdgeInsets.all(8.0),
                      child: Row(
                        children: <Widget>[
                          Container(
                            height: 15.0,
                            width: 15.0,
                            decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              color: const Color(0xFFF50057),
                              //F50057
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.only(right: 8.0),
                          ),
                          Text(
                            "सम्बन्धित समाचारहरु",
                            style: Theme.of(context).textTheme.title,
                          ),
                          Padding(
                            padding: EdgeInsets.only(right: 8.0),
                          ),
                          Flexible(
                            child: Divider(
                              color: Colors.grey,
                              height: 5.0,
                            ),
                          ),
                        ],
                      ),
                    ),
                  )
                : SliverToBoxAdapter(),
            relatedNews.isNotEmpty
                ? SliverList(
                    delegate: SliverChildBuilderDelegate((context, index) {
                      return InkWell(
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => NewsDetailPage(
                                        newsItem: relatedNews[index],
                                      )));
                        },
                        child: Container(
                          height: 270.0,
                          margin: EdgeInsets.only(
                            bottom: 4.0,
                          ),
                          child: Stack(
                            fit: StackFit.expand,
                            children: <Widget>[
                              Hero(
                                tag: "hero-image${relatedNews[index].id}",
                                child: Container(
                                  height: 270.0,
                                  child: CachedNetworkImage(
                                    imageUrl: AppText.prefixImage +
                                        relatedNews[index].featuredImage,
                                    placeholder: (_, url) => new Image.asset(
                                      "assets/defaultImage.jpg",
                                      fit: BoxFit.cover,
                                    ),
                                    errorWidget: (_, url, error) =>
                                        new Image.asset(
                                      "assets/defaultImage.jpg",
                                      fit: BoxFit.cover,
                                    ),
                                    fadeOutDuration: new Duration(seconds: 1),
                                    fadeInDuration: new Duration(seconds: 2),
                                    fit: BoxFit.cover,
                                  ),
                                ),
                              ),
                              Container(
                                decoration: BoxDecoration(
                                  gradient: LinearGradient(
                                    begin: Alignment.topCenter,
                                    end: Alignment.bottomCenter,
                                    colors: [
                                      const Color(0x00000000),
                                      const Color(0x00000000),
                                      const Color(0x00000000),
                                      const Color(0x99000000),
                                      const Color(0xE6000000),
                                      const Color(0xE6000000),
                                    ],
                                  ),
                                ),
                              ),
                              Positioned(
                                bottom: 0.0,
                                left: 0.0,
                                right: 0.0,
                                child: Container(
                                  padding: EdgeInsets.all(8.0),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Text(
                                        relatedNews[index].title,
                                        maxLines: 2,
                                        style: TextStyle(
                                          color: Colors.white,
                                          fontSize: 18.0,
                                        ),
                                      ),
                                      Row(
                                        children: <Widget>[
                                          Text(
                                            relatedNews[index].authorName ==
                                                    "null"
                                                ? relatedNews[index].publishOn
                                                : relatedNews[index]
                                                        .authorName +
                                                    "   |   " +
                                                    relatedNews[index]
                                                        .publishOn,
                                            style: TextStyle(
                                              color: Colors.white,
                                              fontSize: 15.0,
                                            ),
                                          ),
                                          Flexible(
                                            child: Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.end,
                                              children: <Widget>[
                                                IconButton(
                                                  padding: EdgeInsets.all(0.0),
                                                  icon: Icon(
                                                      Icons.bookmark_border),
                                                  color: Colors.white,
                                                  onPressed: () {},
                                                ),
                                                IconButton(
                                                  padding: EdgeInsets.all(0.0),
                                                  icon: Icon(Icons.share),
                                                  color: Colors.white,
                                                  onPressed: () {},
                                                ),
                                              ],
                                            ),
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      );
                    }, childCount: relatedNews.length),
                  )
                : SliverToBoxAdapter(),
          ],
        ),
      ),
    );
  }
}
