import 'dart:async';
import 'dart:convert';

import 'package:annapurna_app/apputil.dart';
import 'package:annapurna_app/chooseCategoryPage.dart';
import 'package:annapurna_app/homePage.dart';
import 'package:annapurna_app/model/categoryItem.dart';
import 'package:annapurna_app/stateContainer/stateContainer.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class ChooseCategoryRoute extends CupertinoPageRoute {
  ChooseCategoryRoute()
      : super(builder: (BuildContext context) => new ChooseCategoryIntroPage());

  @override
  Widget buildPage(BuildContext context, Animation<double> animation,
      Animation<double> secondaryAnimation) {
    return new FadeTransition(
        opacity: animation, child: new ChooseCategoryIntroPage());
  }
}

class ChooseCategoryIntroPage extends StatefulWidget {
  @override
  _ChooseCategoryIntroPageState createState() =>
      _ChooseCategoryIntroPageState();
}

class _ChooseCategoryIntroPageState extends State<ChooseCategoryIntroPage> {
  _getCategory() async {
    bool isConnected = await AppUtil.checkConnection();
    var container = StateContainer.of(context);

    print(isConnected);
    if (isConnected) {
      http
          .get("http://bg.annapurnapost.com/api/category/list?_format=json")
          .then((response) {
        print("Response Body:${response.body}");
        if (response.statusCode == 200) {
          List<CategoryItem> categoryList = new List();
          var responseBody = json.decode(response.body);
          if (responseBody['status'] == "success") {
            var categories = responseBody['data'];
            for (int i = 0; i < categories.length; i++) {
              categoryList.add(new CategoryItem(
                id: categories[i]['id'],
                alias: categories[i]['alias'],
                name: categories[i]['name'],
                choose: false,
              ));
            }

            container.updateCategoryList(categoryList);
          }
        }
      });
    } else {
      print("No internet connection");
    }
  }

  _navigateToChooseCategory() async {
    await Navigator.push(
        context, MaterialPageRoute(builder: (context) => ChooseCategoryPage()));
    Navigator.pushReplacement(
        context, MaterialPageRoute(builder: (context) => Homepage()));
  }

  @override
  void initState() {
    new Future.delayed(Duration.zero, () {
      _getCategory();
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0.0,
      ),
      body: Container(
        padding: EdgeInsets.all(16.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Icon(
                  Icons.add_circle_outline,
                  color: Colors.blue,
                  size: 45.0,
                ),
                Flexible(
                  child: Container(
                    margin: EdgeInsets.only(left: 8.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          "व्यक्तिग न्यूज फिडका लागि आफ्नो रुचीका विषय छान्नुहोस् ।",
                          style: TextStyle(
                            fontSize: 26.0,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(top: 16.0),
                        ),
                        Text(
                          "कृपया कम्तिमा ५ वटा विषय छान्नुहोस् ।",
                          style: TextStyle(
                            fontSize: 22.0,
                          ),
                        ),
                      ],
                    ),
                  ),
                )
              ],
            ),
            Padding(
              padding: EdgeInsets.only(top: 36.0),
            ),
            Align(
              alignment: Alignment.center,
              child: MaterialButton(
                color: Colors.blue,
                textColor: Colors.white,
                minWidth: 250.0,
                height: 50.0,
                onPressed: _navigateToChooseCategory,
                child: Text(
                  "सुरु गरौँ",
                  style: TextStyle(
                    fontSize: 20.0,
                  ),
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(top: 8.0),
            ),
            Align(
              alignment: Alignment.center,
              child: FlatButton(
                child: Text(
                  "पछि गरौँला",
                  style: TextStyle(
                    decoration: TextDecoration.underline,
                    fontSize: 16.0,
                  ),
                ),
                onPressed: () {
                  Navigator.pushReplacement(context,
                      MaterialPageRoute(builder: (context) => Homepage()));
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
