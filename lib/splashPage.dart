import 'dart:async';
import 'dart:convert';

import 'package:annapurna_app/chooseCategoryIntroPage.dart';
import 'package:annapurna_app/homePage.dart';
import 'package:annapurna_app/model/categoryItem.dart';
import 'package:annapurna_app/stateContainer/stateContainer.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SplashPage extends StatefulWidget {
  @override
  _SplashPageState createState() => new _SplashPageState();
}

class _SplashPageState extends State<SplashPage> {
  Future<SharedPreferences> _prefs = SharedPreferences.getInstance();

  Future<List<CategoryItem>> _getCategoryList() async {
    List<CategoryItem> cartItemList = new List();
    final SharedPreferences prefs = await _prefs;
    String cartListString = prefs.getString('#ANC') ?? null;
    if (cartListString != null) {
      json
          .decode(cartListString)
          .forEach((map) => cartItemList.add(new CategoryItem.fromJson(map)));
    }
    return cartItemList;
  }

  Future<bool> checkIsFirstTimeRun() async {
    List<CategoryItem> list = await _getCategoryList();
    var container = StateContainer.of(context);
    container.updateCategoryList(list);
    bool isFirstTime = list.length > 0;
    return isFirstTime;
  }

  @override
  void initState() {
    new Future.delayed(new Duration(seconds: 3), () {
      navigationPage();
    });
    super.initState();
  }

  void navigationPage() async {
    bool isFirstTime = await checkIsFirstTimeRun();

    if (isFirstTime == false) {
      Navigator.pushReplacement(context, new ChooseCategoryRoute());
    } else {
      Navigator.pushReplacement(
          context, MaterialPageRoute(builder: (context) => new Homepage()));
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
      decoration: BoxDecoration(
        image: DecorationImage(
          image: AssetImage("assets/appSplash.gif"),
          fit: BoxFit.cover,
        ),
      ),
    ));
  }
}
