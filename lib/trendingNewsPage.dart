import 'dart:async';
import 'dart:convert';

import 'package:annapurna_app/appText.dart';
import 'package:annapurna_app/apputil.dart';
import 'package:annapurna_app/model/newsItem.dart';
import 'package:annapurna_app/newsDetailPage.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:cached_network_image/cached_network_image.dart';

class TrendingNewsPage extends StatefulWidget {
  TrendingNewsPage({Key key}) : super(key: key);
  @override
  _TrendingNewsPageState createState() => _TrendingNewsPageState();
}

class _TrendingNewsPageState extends State<TrendingNewsPage> {
  List<NewsItem> list = new List();

  Future<Null> _getTrendingNews() async {
    bool isConnected = await AppUtil.checkConnection();
    final Completer<Null> completer = new Completer<Null>();
    print(isConnected);
    if (isConnected) {
      print("Reached Here Trending");
      http.get(AppText.baseURl + "trending/news?_format=json").then((response) {
        print("Response Body:${response.body}");
        if (response.statusCode == 200) {
          List<NewsItem> newsList = new List();
          var responseBody = json.decode(response.body);
          var news = responseBody['data'];
          for (int i = 0; i < news.length; i++) {
            newsList.add(new NewsItem(
              id: int.parse(news[i]['id']),
              title: news[i]['title'],
              introText: news[i]['introText'],
              featuredImage: news[i]['featuredImage'],
              videoLink: news[i]['videoLink'],
              publishOn: news[i]['publishOn'],
              authorName: news[i]['author']['name'],
              categoryName: news[i]['categories'].isNotEmpty
                  ? news[i]['categories'][0]['name']
                  : "",
            ));
          }
          if (mounted) {
            this.setState(() {
              list = newsList;
              completer.complete(null);
            });
          }
          return completer.future;
        }
      });
    } else {
      print("No internet connection");
      return completer.future;
    }
  }

  @override
  void initState() {
    _getTrendingNews();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var yOffset =
        NestedScrollView.sliverOverlapAbsorberHandleFor(context).layoutExtent;

    return Transform.translate(
      offset: Offset(0.0, yOffset),
      child: RefreshIndicator(
        onRefresh: _getTrendingNews,
        child: Transform.translate(
          offset: Offset(0.0, -yOffset),
          child: CustomScrollView(
            slivers: <Widget>[
              SliverOverlapInjector(
                handle:
                    NestedScrollView.sliverOverlapAbsorberHandleFor(context),
              ),
              SliverList(
                delegate: SliverChildBuilderDelegate((context, index) {
                  if (index == 0) {
                    return InkWell(
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => NewsDetailPage(
                                      newsItem: list[index],
                                    )));
                      },
                      child: Container(
                        height: 270.0,
                        margin: EdgeInsets.only(
                          bottom: 4.0,
                        ),
                        child: Stack(
                          fit: StackFit.expand,
                          children: <Widget>[
                            Hero(
                              tag: "hero-image${list[index].id}",
                              child: CachedNetworkImage(
                                imageUrl:
                                    AppText.prefixImage + list[0].featuredImage,
                                placeholder: (_, url) => new Image.asset(
                                  "assets/defaultImage.jpg",
                                  fit: BoxFit.cover,
                                ),
                                errorWidget: (_, url, error) => new Image.asset(
                                  "assets/defaultImage.jpg",
                                  fit: BoxFit.cover,
                                ),
                                fadeOutDuration: new Duration(seconds: 1),
                                fadeInDuration: new Duration(seconds: 2),
                                fit: BoxFit.cover,
                              ),
                            ),
                            Container(
                              decoration: BoxDecoration(
                                gradient: LinearGradient(
                                  begin: Alignment.topCenter,
                                  end: Alignment.bottomCenter,
                                  colors: [
                                    const Color(0x00000000),
                                    const Color(0x00000000),
                                    const Color(0x00000000),
                                    const Color(0x99000000),
                                    //const Color(0xB3000000),
                                    const Color(0xE6000000),
                                    const Color(0xE6000000),
                                  ],
                                ),
                              ),
                            ),
                            Positioned(
                              bottom: 0.0,
                              left: 0.0,
                              right: 0.0,
                              child: Container(
                                padding: EdgeInsets.all(8.0),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    list[index].categoryName.isNotEmpty
                                        ? Container(
                                            color: Colors.red,
                                            padding: EdgeInsets.only(
                                              top: 4.0,
                                              left: 4.0,
                                              right: 4.0,
                                            ),
                                            child: Text(
                                              list[index].categoryName,
                                              //textAlign: TextAlign.center,
                                              style: TextStyle(
                                                color: Colors.white,
                                                fontSize: 15.0,
                                              ),
                                            ),
                                          )
                                        : Container(),
                                    Padding(
                                      padding: EdgeInsets.only(
                                        top: 3.0,
                                      ),
                                    ),
                                    Text(
                                      list[index].title,
                                      maxLines: 2,
                                      style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 18.0,
                                      ),
                                    ),
                                    Row(
                                      children: <Widget>[
                                        Text(
                                          list[index].authorName +
                                              "   |   " +
                                              list[index].publishOn,
                                          style: TextStyle(
                                            color: Colors.white,
                                            fontSize: 15.0,
                                          ),
                                        ),
                                        Flexible(
                                          child: Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.end,
                                            children: <Widget>[
                                              IconButton(
                                                padding: EdgeInsets.all(0.0),
                                                icon:
                                                    Icon(Icons.bookmark_border),
                                                color: Colors.white,
                                                onPressed: () {},
                                              ),
                                              IconButton(
                                                padding: EdgeInsets.all(0.0),
                                                icon: Icon(Icons.share),
                                                color: Colors.white,
                                                onPressed: () {},
                                              ),
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            Positioned(
                              top: 0.0,
                              left: 0.0,
                              child: Container(
                                height: 30.0,
                                width: 30.0,
                                color: Colors.red,
                                child: Center(
                                    child: Text(
                                  "१",
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 16.0,
                                  ),
                                )),
                              ),
                            ),
                          ],
                        ),
                      ),
                    );
                  } else {
                    return InkWell(
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => NewsDetailPage(
                                      newsItem: list[index],
                                    )));
                      },
                      child: Card(
                        elevation: 2.0,
                        child: Stack(
                          children: <Widget>[
                            Container(
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: <Widget>[
                                  Hero(
                                    tag: "hero-image${list[index].id}",
                                    child: Container(
                                      height: 90.0,
                                      width: 120.0,
                                      child: CachedNetworkImage(
                                        imageUrl: AppText.prefixImage +
                                            list[index].featuredImage,
                                        placeholder: (_, url) =>
                                            new Image.asset(
                                          "assets/defaultImage.jpg",
                                          fit: BoxFit.cover,
                                        ),
                                        errorWidget: (_, url, error) =>
                                            new Image.asset(
                                          "assets/defaultImage.jpg",
                                          fit: BoxFit.cover,
                                        ),
                                        fadeOutDuration:
                                            new Duration(seconds: 1),
                                        fadeInDuration:
                                            new Duration(seconds: 2),
                                        fit: BoxFit.cover,
                                      ),
                                    ),
                                  ),
                                  Flexible(
                                    child: Padding(
                                      padding: EdgeInsets.all(8.0),
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: <Widget>[
                                          Text(
                                            list[index].title,
                                            overflow: TextOverflow.ellipsis,
                                            maxLines: 2,
                                            style: TextStyle(
                                              fontSize: 16.0,
                                            ),
                                          ),
                                          RichText(
                                            text: TextSpan(
                                              children: [
                                                TextSpan(
                                                  text:
                                                      list[index].categoryName,
                                                  style: TextStyle(
                                                    color: Colors.blue,
                                                  ),
                                                ),
                                                TextSpan(
                                                  text: "   |   ",
                                                  style: TextStyle(
                                                    color:
                                                        const Color(0xFF808080),
                                                  ),
                                                ),
                                                TextSpan(
                                                  text: list[index].publishOn,
                                                  style: TextStyle(
                                                    color:
                                                        const Color(0xFF808080),
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Positioned(
                              top: 0.0,
                              left: 0.0,
                              child: Container(
                                height: 30.0,
                                width: 30.0,
                                color: Colors.red,
                                child: Center(
                                    child: Text(
                                  index == 1
                                      ? "२"
                                      : index == 2
                                          ? "३ "
                                          : index == 3
                                              ? "४"
                                              : index == 4
                                                  ? "५"
                                                  : index == 5
                                                      ? "६"
                                                      : index == 6
                                                          ? "७"
                                                          : index == 7
                                                              ? "८"
                                                              : index == 8
                                                                  ? "९"
                                                                  : index == 9
                                                                      ? "१०"
                                                                      : index ==
                                                                              10
                                                                          ? "११"
                                                                          : index == 11
                                                                              ? "१२"
                                                                              : index == 12 ? "१३" : "१४",
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 16.0,
                                  ),
                                )),
                              ),
                            ),
                          ],
                        ),
                      ),
                    );
                  }
                }, childCount: list.length),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
