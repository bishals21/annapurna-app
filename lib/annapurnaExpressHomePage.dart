import 'dart:async';
import 'dart:convert';

import 'package:annapurna_app/annapurnaExpressCategoryPage.dart';
import 'package:annapurna_app/annapurnaExpressNewsDetailPage.dart';
import 'package:annapurna_app/appText.dart';
import 'package:annapurna_app/apputil.dart';
import 'package:annapurna_app/model/newsItem.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:http/http.dart' as http;

class AnnapurnaExpressHomePage extends StatefulWidget {
  @override
  _AnnapurnaExpressHomePageState createState() =>
      _AnnapurnaExpressHomePageState();
}

class _AnnapurnaExpressHomePageState extends State<AnnapurnaExpressHomePage> {
  List<NewsItem> breakingNewsList = new List();
  List<NewsItem> trendingNewsList = new List();

  var categoryData;

  Future<Null> _handleRefresh() async {
    print('_handleRefresh');
    final Completer<Null> completer = new Completer<Null>();

    List<NewsItem> breakingListOfItem = new List();
    List<NewsItem> trendingListOfItem = new List();

    bool isConnected = await AppUtil.checkConnection();
    if (isConnected) {
      http
          .get(
        AppText.aEBaseURl + "v1/landing?_format=json",
      )
          .then((response) {
        if (response.statusCode == 200) {
          var responseBody = json.decode(response.body);
          print(responseBody.toString());
          var data = responseBody["data"]["contents"];
          var breakingNews = data["breakingNews"]["data"];
          var trendingNews = data["trending"]["data"];

          for (var item in breakingNews) {
            breakingListOfItem.add(new NewsItem(
              id: item['id'],
              title: item['title'],
              introText: item['introText'],
              featuredImage: item['featuredImage'],
              publishOn: item['publishOn'],
              authorName: item['author']['name'].toString(),
            ));
          }

          for (var item in trendingNews) {
            trendingListOfItem.add(new NewsItem(
              id: int.parse(item['id']),
              title: item['title'],
              introText: item['introText'],
              featuredImage: item['featuredImage'],
              publishOn: item['publishOn'],
              authorName: item['author']['name'].toString(),
            ));
          }
          if (mounted) {
            this.setState(() {
              breakingNewsList = breakingListOfItem;
              trendingNewsList = trendingListOfItem;
              categoryData = data;
              //print(categoryData['interviews']['data'][0]['featuredImage']);
              completer.complete(null);
            });
          }

          return completer.future;
        }
      });
    } else {
      print("No Internet Connection");
      return completer.future;
    }
  }

  @override
  void initState() {
    _handleRefresh();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("The Annapurna Express"),
      ),
      body: ListView(
        children: <Widget>[
          breakingNewsList.isNotEmpty
              ? Container(
                  height: 270.0,
                  child: Swiper(
                    containerHeight: 270.0,
                    itemCount: breakingNewsList.length,
                    autoplay: true,
                    itemBuilder: (context, index) {
                      return InkWell(
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) =>
                                      AnnapurnaExpressNewsDetailPage(
                                        newsItem: breakingNewsList[index],
                                      )));
                        },
                        child: Container(
                          height: 270.0,
                          margin: EdgeInsets.only(
                            bottom: 4.0,
                          ),
                          child: Stack(
                            children: <Widget>[
                              Container(
                                height: 270.0,
                                child: CachedNetworkImage(
                                  imageUrl: AppText.aEPrefixImage +
                                      breakingNewsList[index].featuredImage,
                                  placeholder: (_, url) =>new Image.asset(
                                    "assets/defaultImage.jpg",
                                    fit: BoxFit.cover,
                                  ),
                                  errorWidget:(_, url,error) => new Image.asset(
                                    "assets/defaultImage.jpg",
                                    fit: BoxFit.cover,
                                  ),
                                  fadeOutDuration: new Duration(seconds: 1),
                                  fadeInDuration: new Duration(seconds: 2),
                                  fit: BoxFit.cover,
                                ),
                              ),
                              Container(
                                decoration: BoxDecoration(
                                  gradient: LinearGradient(
                                    begin: Alignment.topCenter,
                                    end: Alignment.bottomCenter,
                                    colors: [
                                      const Color(0x00000000),
                                      const Color(0x00000000),
                                      const Color(0x00000000),
                                      const Color(0x99000000),
                                      const Color(0xE6000000),
                                      const Color(0xE6000000),
                                    ],
                                  ),
                                ),
                              ),
                              Positioned(
                                bottom: 0.0,
                                left: 0.0,
                                right: 0.0,
                                child: Container(
                                  padding: EdgeInsets.all(8.0),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Text(
                                        breakingNewsList[index].title,
                                        maxLines: 2,
                                        style: TextStyle(
                                          color: Colors.white,
                                          fontSize: 18.0,
                                        ),
                                      ),
                                      Row(
                                        children: <Widget>[
                                          Text(
                                            breakingNewsList[index]
                                                .publishOn
                                                .substring(
                                                    0,
                                                    breakingNewsList[index]
                                                        .publishOn
                                                        .indexOf(" ")),
                                            style: TextStyle(
                                              color: Colors.white,
                                              fontSize: 15.0,
                                            ),
                                          ),
                                          Flexible(
                                            child: Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.end,
                                              children: <Widget>[
                                                IconButton(
                                                  padding: EdgeInsets.all(0.0),
                                                  icon: Icon(
                                                      Icons.bookmark_border),
                                                  color: Colors.white,
                                                  onPressed: () {},
                                                ),
                                                IconButton(
                                                  padding: EdgeInsets.all(0.0),
                                                  icon: Icon(Icons.share),
                                                  color: Colors.white,
                                                  onPressed: () {},
                                                ),
                                              ],
                                            ),
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      );
                    },
                  ),
                )
              : Container(),
          ListView.builder(
            itemBuilder: (context, index) {
              return InkWell(
                onTap: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => AnnapurnaExpressNewsDetailPage(
                                newsItem: trendingNewsList[index],
                              )));
                },
                child: Card(
                  elevation: 2.0,
                  child: Container(
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Container(
                          height: 90.0,
                          width: 120.0,
                          child: CachedNetworkImage(
                            imageUrl: AppText.aEPrefixImage +
                                trendingNewsList[index].featuredImage,
                            placeholder: (_, url) =>new Image.asset(
                              "assets/defaultImage.jpg",
                              fit: BoxFit.cover,
                            ),
                            errorWidget: (_, url,error) =>new Image.asset(
                              "assets/defaultImage.jpg",
                              fit: BoxFit.cover,
                            ),
                            fadeOutDuration: new Duration(seconds: 1),
                            fadeInDuration: new Duration(seconds: 2),
                            fit: BoxFit.cover,
                          ),
                        ),
                        Flexible(
                          child: Padding(
                            padding: EdgeInsets.all(8.0),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  trendingNewsList[index].title,
                                  overflow: TextOverflow.ellipsis,
                                  maxLines: 2,
                                  style: TextStyle(
                                    fontSize: 16.0,
                                    color: const Color(0xFF4D4E4E),
                                  ),
                                ),
                                Padding(
                                  padding: EdgeInsets.only(
                                    top: 4.0,
                                  ),
                                ),
                                RichText(
                                  text: TextSpan(
                                    children: [
                                      TextSpan(
                                        text: trendingNewsList[index]
                                            .publishOn
                                            .substring(
                                                0,
                                                trendingNewsList[index]
                                                    .publishOn
                                                    .indexOf(" ")),
                                        style: TextStyle(
                                          color: Colors.blue,
                                        ),
                                      ),
                                      TextSpan(
                                        text: "   |   ",
                                        style: TextStyle(
                                          color: const Color(0xFF808080),
                                        ),
                                      ),
                                      TextSpan(
                                        text: trendingNewsList[index]
                                                    .authorName ==
                                                "null"
                                            ? ""
                                            : trendingNewsList[index]
                                                .authorName,
                                        style: TextStyle(
                                          color: const Color(0xFF808080),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              );
            },
            itemCount: 5,
            shrinkWrap: true,
            physics: ClampingScrollPhysics(),
          ),
          categoryData != null
              ? Container(
                  height: 160.0,
                  margin: EdgeInsets.all(4.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      Expanded(
                        child: InkWell(
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) =>
                                        AnnapurnaExpressCategoryPage(
                                          alias: "editorial",
                                          name: "Editorial",
                                        )));
                          },
                          child: Container(
                            margin: EdgeInsets.only(
                              right: 4.0,
                            ),
                            decoration: BoxDecoration(
                              color: Colors.white,
                              boxShadow: [
                                BoxShadow(
                                  color: const Color(0x26000000),
                                  blurRadius: 3.0,
                                  spreadRadius: 1.0,
                                  offset: new Offset(1.0, 0.0),
                                ),
                              ],
                            ),
                            child: Column(
                              children: <Widget>[
                                Expanded(
                                  child: Stack(
                                    fit: StackFit.expand,
                                    children: <Widget>[
                                      Container(
                                        height: 130.0,
                                        child: CachedNetworkImage(
                                          imageUrl: AppText.aEPrefixImage +
                                              categoryData['editorials']['data']
                                                      [0]['featuredImage']
                                                  .toString(),
                                          placeholder: (_, url) =>new Image.asset(
                                            "assets/expressLogo.png",
                                            //fit: BoxFit.cover,
                                          ),
                                          errorWidget:(_, url,error) => new Image.asset(
                                            "assets/expressLogo.png",
                                            //fit: BoxFit.cover,
                                          ),
                                          fadeOutDuration:
                                              new Duration(seconds: 1),
                                          fadeInDuration:
                                              new Duration(seconds: 2),
                                          fit: BoxFit.cover,
                                        ),
                                      ),
                                      Positioned(
                                        bottom: 0.0,
                                        left: 0.0,
                                        right: 0.0,
                                        child: Container(
                                          padding: EdgeInsets.all(4.0),
                                          child: Text(
                                            categoryData['editorials']['data']
                                                [0]['title'],
                                            maxLines: 1,
                                            overflow: TextOverflow.ellipsis,
                                            style: TextStyle(
                                              color: Colors.white,
                                            ),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                Container(
                                  height: 30.0,
                                  padding: EdgeInsets.only(
                                    left: 4.0,
                                  ),
                                  alignment: Alignment.centerLeft,
                                  child: Text(
                                    "EDITORIAL",
                                    overflow: TextOverflow.ellipsis,
                                    style: TextStyle(
                                      color: Colors.blue,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                      Expanded(
                        child: InkWell(
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) =>
                                        AnnapurnaExpressCategoryPage(
                                          alias: "interview",
                                          name: "Interview",
                                        )));
                          },
                          child: Container(
                            margin: EdgeInsets.only(
                              left: 4.0,
                            ),
                            decoration: BoxDecoration(
                              color: Colors.white,
                              boxShadow: [
                                BoxShadow(
                                  color: const Color(0x26000000),
                                  blurRadius: 3.0,
                                  spreadRadius: 1.0,
                                  offset: new Offset(1.0, 0.0),
                                ),
                              ],
                            ),
                            child: Column(
                              children: <Widget>[
                                Expanded(
                                  child: Stack(
                                    fit: StackFit.expand,
                                    children: <Widget>[
                                      Container(
                                        height: 130.0,
                                        child: CachedNetworkImage(
                                          imageUrl: AppText.aEPrefixImage +
                                              categoryData['interviews']['data']
                                                      [0]['featuredImage']
                                                  .toString(),
                                          placeholder:(_, url) => new Image.asset(
                                            "assets/expressLogo.png",
                                            fit: BoxFit.cover,
                                          ),
                                          errorWidget: (_, url,error) =>new Image.asset(
                                            "assets/expressLogo.png",
                                            fit: BoxFit.cover,
                                          ),
                                          fadeOutDuration:
                                              new Duration(seconds: 1),
                                          fadeInDuration:
                                              new Duration(seconds: 2),
                                          fit: BoxFit.cover,
                                        ),
                                      ),
                                      Positioned(
                                        bottom: 0.0,
                                        left: 0.0,
                                        right: 0.0,
                                        child: Container(
                                          padding: EdgeInsets.all(4.0),
                                          child: Text(
                                            categoryData['interviews']['data']
                                                [0]['title'],
                                            maxLines: 1,
                                            overflow: TextOverflow.ellipsis,
                                            style: TextStyle(
                                              color: Colors.white,
                                            ),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                Container(
                                  height: 30.0,
                                  padding: EdgeInsets.only(
                                    left: 4.0,
                                  ),
                                  alignment: Alignment.centerLeft,
                                  child: Text(
                                    "INTERVIEW",
                                    overflow: TextOverflow.ellipsis,
                                    style: TextStyle(
                                      color: Colors.blue,
                                    ),
                                  ),
                                ),
                                // Container(
                                //   padding: EdgeInsets.all(4.0),
                                //   child: Text(
                                //     categoryData['interviews']['data'][0]
                                //         ['title'],
                                //     maxLines: 2,
                                //     overflow: TextOverflow.ellipsis,
                                //     style: TextStyle(
                                //       color: const Color(0xFF4D4E4E),
                                //     ),
                                //   ),
                                // ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                )
              : Container(),
          categoryData != null
              ? Container(
                  height: 160.0,
                  margin: EdgeInsets.all(4.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      Expanded(
                        child: InkWell(
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) =>
                                        AnnapurnaExpressCategoryPage(
                                          alias: "features",
                                          name: "Features",
                                        )));
                          },
                          child: Container(
                            margin: EdgeInsets.only(
                              right: 4.0,
                            ),
                            decoration: BoxDecoration(
                              color: Colors.white,
                              boxShadow: [
                                BoxShadow(
                                  color: const Color(0x26000000),
                                  blurRadius: 3.0,
                                  spreadRadius: 1.0,
                                  offset: new Offset(1.0, 0.0),
                                ),
                              ],
                            ),
                            child: Column(
                              children: <Widget>[
                                Expanded(
                                  child: Stack(
                                    fit: StackFit.expand,
                                    children: <Widget>[
                                      Container(
                                        height: 130.0,
                                        child: CachedNetworkImage(
                                          imageUrl: AppText.aEPrefixImage +
                                              categoryData['features']['data']
                                                      [0]['featuredImage']
                                                  .toString(),
                                          placeholder: (_, url) =>new Image.asset(
                                            "assets/expressLogo.png",
                                            fit: BoxFit.cover,
                                          ),
                                          errorWidget:(_, url,error) => new Image.asset(
                                            "assets/expressLogo.png",
                                            fit: BoxFit.cover,
                                          ),
                                          fadeOutDuration:
                                              new Duration(seconds: 1),
                                          fadeInDuration:
                                              new Duration(seconds: 2),
                                          fit: BoxFit.cover,
                                        ),
                                      ),
                                      Positioned(
                                        bottom: 0.0,
                                        left: 0.0,
                                        right: 0.0,
                                        child: Container(
                                          padding: EdgeInsets.all(4.0),
                                          child: Text(
                                            categoryData['features']['data'][0]
                                                ['title'],
                                            maxLines: 1,
                                            overflow: TextOverflow.ellipsis,
                                            style: TextStyle(
                                              color: Colors.white,
                                            ),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                Container(
                                  padding: EdgeInsets.all(4.0),
                                  alignment: Alignment.centerLeft,
                                  child: Text(
                                    "FEATURES",
                                    overflow: TextOverflow.ellipsis,
                                    style: TextStyle(
                                      color: Colors.blue,
                                    ),
                                  ),
                                ),

                                // Container(
                                //   padding: EdgeInsets.all(4.0),
                                //   child: Text(
                                //     categoryData['features']['data'][0]
                                //         ['title'],
                                //     maxLines: 2,
                                //     overflow: TextOverflow.ellipsis,
                                //     style: TextStyle(
                                //       color: const Color(0xFF4D4E4E),
                                //     ),
                                //   ),
                                // ),
                              ],
                            ),
                          ),
                        ),
                      ),
                      Expanded(
                        child: InkWell(
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) =>
                                        AnnapurnaExpressCategoryPage(
                                          alias: "politics",
                                          name: "Politics",
                                        )));
                          },
                          child: Container(
                            margin: EdgeInsets.only(
                              left: 4.0,
                            ),
                            decoration: BoxDecoration(
                              color: Colors.white,
                              boxShadow: [
                                BoxShadow(
                                  color: const Color(0x26000000),
                                  blurRadius: 3.0,
                                  spreadRadius: 1.0,
                                  offset: new Offset(1.0, 0.0),
                                ),
                              ],
                            ),
                            child: Column(
                              children: <Widget>[
                                Expanded(
                                  child: Stack(
                                    fit: StackFit.expand,
                                    children: <Widget>[
                                      Container(
                                        height: 130.0,
                                        child: CachedNetworkImage(
                                          imageUrl: AppText.aEPrefixImage +
                                              categoryData['politics']['data']
                                                      [0]['featuredImage']
                                                  .toString(),
                                          placeholder: (_, url) =>new Image.asset(
                                            "assets/expressLogo.png",
                                            fit: BoxFit.cover,
                                          ),
                                          errorWidget: (_, url,error) =>new Image.asset(
                                            "assets/expressLogo.png",
                                            fit: BoxFit.cover,
                                          ),
                                          fadeOutDuration:
                                              new Duration(seconds: 1),
                                          fadeInDuration:
                                              new Duration(seconds: 2),
                                          fit: BoxFit.cover,
                                        ),
                                      ),
                                      Positioned(
                                        bottom: 0.0,
                                        left: 0.0,
                                        right: 0.0,
                                        child: Container(
                                          padding: EdgeInsets.all(4.0),
                                          child: Text(
                                            categoryData['politics']['data'][0]
                                                ['title'],
                                            maxLines: 1,
                                            overflow: TextOverflow.ellipsis,
                                            style: TextStyle(
                                              color: Colors.white,
                                            ),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                Container(
                                  padding: EdgeInsets.all(4.0),
                                  alignment: Alignment.centerLeft,
                                  child: Text(
                                    "POLITICS",
                                    overflow: TextOverflow.ellipsis,
                                    style: TextStyle(
                                      color: Colors.blue,
                                    ),
                                  ),
                                ),
                                // Container(
                                //   padding: EdgeInsets.all(4.0),
                                //   child: Text(
                                //     categoryData['politics']['data'][0]
                                //         ['title'],
                                //     maxLines: 2,
                                //     overflow: TextOverflow.ellipsis,
                                //     style: TextStyle(
                                //       color: const Color(0xFF4D4E4E),
                                //     ),
                                //   ),
                                // ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                )
              : Container(),
          categoryData != null
              ? Container(
                  height: 160.0,
                  margin: EdgeInsets.all(4.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      Expanded(
                        child: InkWell(
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) =>
                                        AnnapurnaExpressCategoryPage(
                                          alias: "opinion",
                                          name: "Opinions",
                                        )));
                          },
                          child: Container(
                            margin: EdgeInsets.only(
                              right: 4.0,
                            ),
                            decoration: BoxDecoration(
                              color: Colors.white,
                              boxShadow: [
                                BoxShadow(
                                  color: const Color(0x26000000),
                                  blurRadius: 3.0,
                                  spreadRadius: 1.0,
                                  offset: new Offset(1.0, 0.0),
                                ),
                              ],
                            ),
                            child: Column(
                              children: <Widget>[
                                Expanded(
                                  child: Stack(
                                    fit: StackFit.expand,
                                    children: <Widget>[
                                      Container(
                                        height: 130.0,
                                        child: CachedNetworkImage(
                                          imageUrl: AppText.aEPrefixImage +
                                              categoryData['opinions']['data']
                                                      [0]['featuredImage']
                                                  .toString(),
                                          placeholder:(_, url) => new Image.asset(
                                            "assets/expressLogo.png",
                                            fit: BoxFit.cover,
                                          ),
                                          errorWidget: (_, url,error) =>new Image.asset(
                                            "assets/expressLogo.png",
                                            fit: BoxFit.cover,
                                          ),
                                          fadeOutDuration:
                                              new Duration(seconds: 1),
                                          fadeInDuration:
                                              new Duration(seconds: 2),
                                          fit: BoxFit.cover,
                                        ),
                                      ),
                                      Positioned(
                                        bottom: 0.0,
                                        left: 0.0,
                                        right: 0.0,
                                        child: Container(
                                          padding: EdgeInsets.all(4.0),
                                          child: Text(
                                            categoryData['opinions']['data'][0]
                                                ['title'],
                                            maxLines: 1,
                                            overflow: TextOverflow.ellipsis,
                                            style: TextStyle(
                                              color: Colors.white,
                                            ),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                Container(
                                  padding: EdgeInsets.all(4.0),
                                  alignment: Alignment.centerLeft,
                                  child: Text(
                                    "OPINIONS",
                                    overflow: TextOverflow.ellipsis,
                                    style: TextStyle(
                                      color: Colors.blue,
                                    ),
                                  ),
                                ),
                                // Container(
                                //   padding: EdgeInsets.all(4.0),
                                //   child: Text(
                                //     categoryData['opinions']['data'][0]
                                //         ['title'],
                                //     maxLines: 2,
                                //     overflow: TextOverflow.ellipsis,
                                //     style: TextStyle(
                                //       color: const Color(0xFF4D4E4E),
                                //     ),
                                //   ),
                                // ),
                              ],
                            ),
                          ),
                        ),
                      ),
                      Expanded(
                        child: InkWell(
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) =>
                                        AnnapurnaExpressCategoryPage(
                                          alias: "auto-and-tech",
                                          name: "Auto & Tech",
                                        )));
                          },
                          child: Container(
                            margin: EdgeInsets.only(
                              left: 4.0,
                            ),
                            decoration: BoxDecoration(
                              color: Colors.white,
                              boxShadow: [
                                BoxShadow(
                                  color: const Color(0x26000000),
                                  blurRadius: 3.0,
                                  spreadRadius: 1.0,
                                  offset: new Offset(1.0, 0.0),
                                ),
                              ],
                            ),
                            child: Column(
                              children: <Widget>[
                                Expanded(
                                  child: Stack(
                                    fit: StackFit.expand,
                                    children: <Widget>[
                                      Container(
                                        height: 130.0,
                                        child: CachedNetworkImage(
                                          imageUrl: AppText.aEPrefixImage +
                                              categoryData['autoAndTechs']
                                                          ['data'][0]
                                                      ['featuredImage']
                                                  .toString(),
                                          placeholder:(_, url) => new Image.asset(
                                            "assets/expressLogo.png",
                                            fit: BoxFit.cover,
                                          ),
                                          errorWidget:(_, url,error) => new Image.asset(
                                            "assets/expressLogo.png",
                                            fit: BoxFit.cover,
                                          ),
                                          fadeOutDuration:
                                              new Duration(seconds: 1),
                                          fadeInDuration:
                                              new Duration(seconds: 2),
                                          fit: BoxFit.cover,
                                        ),
                                      ),
                                      Positioned(
                                        bottom: 0.0,
                                        left: 0.0,
                                        right: 0.0,
                                        child: Container(
                                          padding: EdgeInsets.all(4.0),
                                          child: Text(
                                            categoryData['autoAndTechs']['data']
                                                [0]['title'],
                                            maxLines: 1,
                                            overflow: TextOverflow.ellipsis,
                                            style: TextStyle(
                                              color: Colors.white,
                                            ),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                Container(
                                  padding: EdgeInsets.all(4.0),
                                  alignment: Alignment.centerLeft,
                                  child: Text(
                                    "AUTO & TECH",
                                    overflow: TextOverflow.ellipsis,
                                    style: TextStyle(
                                      color: Colors.blue,
                                    ),
                                  ),
                                ),
                                // Container(
                                //   padding: EdgeInsets.all(4.0),
                                //   child: Text(
                                //     categoryData['autoAndTechs']['data'][0]
                                //         ['title'],
                                //     maxLines: 2,
                                //     overflow: TextOverflow.ellipsis,
                                //     style: TextStyle(
                                //       color: const Color(0xFF4D4E4E),
                                //     ),
                                //   ),
                                // ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                )
              : Container(),
        ],
      ),
    );
  }
}
