class NewsItem {
  NewsItem({
    this.id,
    this.title,
    this.introText,
    this.featuredImage,
    this.videoLink,
    this.publishOn,
    this.authorName,
    this.categoryName,
    this.relatedNews,
    this.alias,
    this.isBreaking,
  });
  int id;
  String title;
  String introText;
  String featuredImage;
  String videoLink;
  String publishOn;
  String authorName;
  String categoryName;
  List<NewsItem> relatedNews;
  String alias;
  bool isBreaking;

//   Map<String, dynamic> toJson() {
//     var map = new Map<String, dynamic>();
//     map["id"] = id;
//     map["title"] = title;
//     map["introText"] = introText;
//     map["featuredImage"] = featuredImage;
//     map["videoLink"] = videoLink;
//     map["publishOn"] = publishOn;
//     map["authorName"] = authorName;
//     map["categoryName"] = categoryName;
//     return map;
//   }

//   NewsItem.fromJson(Map<String, dynamic> map)
//       : this(
//           id: map["id"],
//           title: map["title"],
//           introText: map["introText"],
//           featuredImage: map["featuredImage"],
//           videoLink: map["videoLink"],
//           publishOn: map["publishOn"],
//           authorName: map["authorName"],
//           categoryName: map["categoryName"],
//         );
}
