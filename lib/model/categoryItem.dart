class CategoryItem {
  CategoryItem({this.id, this.alias, this.name, this.choose});
  int id;
  String alias;
  String name;
  bool choose;

  Map<String, dynamic> toJson() {
    var map = new Map<String, dynamic>();
    map["id"] = id;
    map["alias"] = alias;
    map["name"] = name;
    map["choose"] = choose;
    return map;
  }

  CategoryItem.fromJson(Map<String, dynamic> map)
      : this(
          id: map["id"],
          alias: map["alias"],
          name: map["name"],
          choose: map["choose"],
        );
}
