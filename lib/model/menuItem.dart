class MenuItem {
  MenuItem({this.title, this.alias});
  String title;
  String alias;

  Map<String, dynamic> toJson() {
    var map = new Map<String, dynamic>();
    map["title"] = title;
    map["alias"] = alias;
    return map;
  }

  MenuItem.fromJson(Map<String, dynamic> map)
      : this(
          title: map["title"],
          alias: map["alias"],
        );
}
