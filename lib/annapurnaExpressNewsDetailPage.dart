import 'dart:async';
import 'dart:convert';

import 'package:annapurna_app/appText.dart';
import 'package:annapurna_app/apputil.dart';
import 'package:annapurna_app/model/newsItem.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:html2md/html2md.dart' as html2md;
import 'package:flutter_markdown/flutter_markdown.dart';

class AnnapurnaExpressNewsDetailPage extends StatefulWidget {
  final NewsItem newsItem;
  AnnapurnaExpressNewsDetailPage({
    Key key,
    this.newsItem,
  }) : super(key: key);
  @override
  _AnnapurnaExpressNewsDetailPageState createState() =>
      _AnnapurnaExpressNewsDetailPageState();
}

class _AnnapurnaExpressNewsDetailPageState
    extends State<AnnapurnaExpressNewsDetailPage> {
  NewsItem newsItem;

  String description = "";
  String pageTitle = "";

  Future<Null> _getNewsDetail() async {
    bool isConnected = await AppUtil.checkConnection();
    final Completer<Null> completer = new Completer<Null>();
    print(isConnected);
    if (isConnected) {
      http
          .get(AppText.aEBaseURl +
              "v1/news/" +
              newsItem.id.toString() +
              "?_format=json")
          .then((response) {
        print("Response Body:${response.body}");
        if (response.statusCode == 200) {
          var responseBody = json.decode(response.body);
          if (mounted) {
            this.setState(() {
              description = html2md.convert(responseBody['news']['content']);
              completer.complete(null);
            });
          }

          return completer.future;
        }
      });
    } else {
      print("No internet connection");
      return completer.future;
    }
  }

  @override
  void initState() {
    newsItem = widget.newsItem;
    _getNewsDetail();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    bool _handleScrollNotification(ScrollNotification notification) {
      double visibleStatsHeight = notification.metrics.pixels;
      double screenHeight = MediaQuery.of(context).size.height -
          MediaQuery.of(context).padding.top;
      double visiblePercentage = visibleStatsHeight / screenHeight;
      pageTitle = visiblePercentage > 0.3 ? newsItem.title : "";
      setState(() {});
      return false;
    }

    return Scaffold(
      backgroundColor: Colors.white,
      body: NotificationListener<ScrollNotification>(
        onNotification: _handleScrollNotification,
        child: CustomScrollView(
          slivers: <Widget>[
            SliverAppBar(
              expandedHeight: 270.0,
              pinned: true,
              title: Text(
                pageTitle,
                maxLines: 1,
                style: TextStyle(
                  color: Colors.white,
                ),
              ),
              actions: <Widget>[
                IconButton(
                  icon: Icon(Icons.bookmark_border),
                  onPressed: () {},
                ),
                IconButton(
                  icon: Icon(Icons.share),
                  onPressed: () {},
                ),
              ],
              flexibleSpace: FlexibleSpaceBar(
                background: Stack(
                  children: <Widget>[
                    Hero(
                      tag: "hero-image${newsItem.id}",
                      child: Container(
                        decoration: BoxDecoration(
                          image: DecorationImage(
                            image: newsItem.featuredImage != null
                                ? CachedNetworkImageProvider(
                                    AppText.aEPrefixImage +
                                        newsItem.featuredImage,
                                  )
                                : AssetImage("assets/defaultImage.jpg"),
                            fit: BoxFit.cover,
                          ),
                        ),
                      ),
                    ),
                    Container(
                      color: Colors.black12,
                    ),
                  ],
                ),
              ),
            ),
            SliverPadding(
              padding: EdgeInsets.symmetric(horizontal: 8.0, vertical: 8.0),
              sliver: SliverList(
                  delegate: SliverChildListDelegate([
                Text(
                  newsItem.authorName == "null"
                      ? newsItem.publishOn
                      : newsItem.authorName + "   |   " + newsItem.publishOn,
                  style: TextStyle(
                    color: const Color(0xFF4D4E4E),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(
                    top: 8.0,
                  ),
                ),
                Text(
                  newsItem.title,
                  style: TextStyle(
                    fontSize: 22.0,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(
                    top: 16.0,
                  ),
                ),
                MarkdownBody(
                  data: description,
                  styleSheet:
                      MarkdownStyleSheet.largeFromTheme(Theme.of(context))
                          .copyWith(
                    horizontalRuleDecoration: new BoxDecoration(
                      border: new Border(
                        top: new BorderSide(
                            width: 2.0, color: Colors.grey.shade300),
                      ),
                    ),
                    //blockSpacing: 16.0,
                  ),
                ),
              ])),
            ),
          ],
        ),
      ),
    );
  }
}
