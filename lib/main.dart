import 'package:annapurna_app/splashPage.dart';
import 'package:annapurna_app/stateContainer/stateContainer.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

void main() {
  SystemChrome.setPreferredOrientations(
      [DeviceOrientation.portraitUp, DeviceOrientation.portraitDown]);
  runApp(new MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      builder: (context, child) {
        return StateContainer(
          child: child,
        );
      },
      theme: new ThemeData(
        primarySwatch: Colors.blue,
        textTheme: TextTheme(
          body1: TextStyle(
            fontSize: 16.0,
          ),
        ),
      ),
      home: new SplashPage(),
      debugShowCheckedModeBanner: false,
    );
  }
}
