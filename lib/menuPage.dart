import 'package:annapurna_app/cartoonPage.dart';
import 'package:annapurna_app/categoryPage.dart';
import 'package:annapurna_app/categoryVideoPage.dart';
import 'package:annapurna_app/horoscopePage.dart';
import 'package:annapurna_app/model/categoryItem.dart';
import 'package:annapurna_app/stateContainer/stateContainer.dart';
import 'package:flutter/material.dart';

class MenuPage extends StatefulWidget {
  @override
  _MenuPageState createState() => _MenuPageState();
}

class _MenuPageState extends State<MenuPage> {
  @override
  Widget build(BuildContext context) {
    var container = StateContainer.of(context);
    List<CategoryItem> categoryList = container.categoryList;
    return Scaffold(
      //backgroundColor: const Color(0xFFCBCBCB),
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0.0,
        leading: IconButton(
          icon: Icon(Icons.close),
          color: Colors.black,
          onPressed: () {
            Navigator.maybePop(context);
          },
        ),
      ),
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage("assets/menuBackground.png"),
            alignment: Alignment.centerRight,
            fit: BoxFit.fitHeight,
          ),
        ),
        child: Column(
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                IconButton(
                  icon: Icon(Icons.search),
                  onPressed: () {},
                  color: Colors.black,
                ),
                Padding(
                  padding: EdgeInsets.symmetric(
                    horizontal: 8.0,
                  ),
                ),
                IconButton(
                  icon: Icon(Icons.settings),
                  onPressed: () {},
                  color: Colors.black,
                ),
                Padding(
                  padding: EdgeInsets.symmetric(
                    horizontal: 8.0,
                  ),
                ),
                IconButton(
                  icon: ImageIcon(
                    AssetImage("assets/epaperLogo.png"),
                    color: Colors.black,
                  ),
                  onPressed: () {},
                ),
              ],
            ),
            Divider(
              color: Colors.grey,
            ),
            Expanded(
              child: ListView(
                children: <Widget>[
                  ListTile(
                    title: Text(
                      "राशिफल",
                      style: TextStyle(
                        fontSize: 16.0,
                      ),
                    ),
                    onTap: () {
                      Navigator.pop(context);
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => HoroscopePage()));
                    },
                  ),
                  ListTile(
                    title: Text(
                      "कार्टून",
                      style: TextStyle(
                        fontSize: 16.0,
                      ),
                    ),
                    onTap: () {
                      Navigator.pop(context);
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => CartoonPage()));
                    },
                  ),
                  ListTile(
                    title: Text(
                      "भिडियो",
                      style: TextStyle(
                        fontSize: 16.0,
                      ),
                    ),
                    onTap: () {
                      Navigator.pop(context);
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => CategoryVideoPage()));
                    },
                  ),
                  Column(
                    children: categoryList.map((item) {
                      return ListTile(
                        title: Text(
                          item.name,
                          style: TextStyle(
                            fontSize: 16.0,
                          ),
                        ),
                        onTap: () {
                          Navigator.pop(context);
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) =>
                                      CategoryPage(item: item)));
                        },
                      );
                    }).toList(),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
