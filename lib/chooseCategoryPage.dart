import 'dart:async';

import 'package:annapurna_app/model/categoryItem.dart';
import 'package:annapurna_app/stateContainer/stateContainer.dart';
import 'package:flutter/material.dart';

class ChooseCategoryPage extends StatefulWidget {
  @override
  _ChooseCategoryPageState createState() => _ChooseCategoryPageState();
}

class _ChooseCategoryPageState extends State<ChooseCategoryPage> {
  List<CategoryItem> selectedList = new List();
  bool isChanged = false;

  @override
  Widget build(BuildContext context) {
    var container = StateContainer.of(context);
    List<CategoryItem> list = container.categoryList;

    checkCategory(int index, bool value) {
      isChanged = true;
      CategoryItem item = list[index];
      item.choose = value;
      list.removeAt(index);
      list.insert(index, item);
      if (value == true) {
        selectedList.add(list[index]);
      } else {
        selectedList.remove(list[index]);
      }
      container.updateCategoryList(list);
    }

    Future<bool> _onWillPop() {
      Navigator.pop(context, isChanged);
      return Future.value(false);
    }

    return WillPopScope(
      onWillPop: _onWillPop,
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.white,
          elevation: 0.0,
          title: Text(
            "विषय छान्नुहोस्",
            style: TextStyle(
              color: Colors.black,
            ),
          ),
          leading: IconButton(
            icon: Icon(Icons.close),
            color: Colors.black,
            onPressed: () => Navigator.pop(context, isChanged),
          ),
        ),
        body: ListView.builder(
          itemCount: list.length,
          itemBuilder: (context, index) {
            return CheckboxListTile(
              title: Text(
                list[index].name,
                style: TextStyle(
                  fontSize: 18.0,
                ),
              ),
              value: list[index].choose,
              onChanged: (value) {
                checkCategory(index, value);
              },
            );
          },
        ),
      ),
    );
  }
}
